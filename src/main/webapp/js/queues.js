$(document).ready(function() {
var tenid = "362781";
var ip = "10.226.14.80";
var port = "6565";
//var ip = "127.0.0.1";
//var port = "8080";
var queues = [];
var cur_queue = null;
var qgraph = [];

// Events
$("#sleft").click(function () { 
	var leftPos = $('#agentslist').scrollLeft();
	$("#agentslist").animate({scrollLeft: leftPos - 200}, 100);
});

$("#sright").click(function () { 
	var leftPos = $('#agentslist').scrollLeft();
	$("#agentslist").animate({scrollLeft: leftPos + 200}, 100);
});

$('#agentslist').on('click','.agentbox',function(){
	$(".agentbox").removeClass('a-active');
	$(this).addClass('a-active');
	drawforce($(this).find('.ext').text());
});

// Web Sockets
var socket = null;
socket = new WebSocket('ws://' + ip + ':' + port + '/sock');

socket.onerror = function(event){
	noty({text: 'Unable to connect!',layout:'bottomRight',theme: 'relax',type:'error'});
}

socket.onclose = function(event){
	noty({text: 'Disconnected!',layout:'bottomRight',theme: 'relax',type:'error'});
}

socket.onopen = function(event){
	var req_agents = JSON.stringify({tenantid:tenid,req:"queues",tid:"100"});
	socket.send(req_agents);
}

socket.onmessage = function(e){
	var data = jQuery.parseJSON(e.data);
	//console.log(data);
	if(data[0] != undefined){
		var alist = $('#agentslist');
		alist.html('');
		$.each(data[0].queues,function(i,e){
			var ext = e.number;
			var name = e.name;
			alist.append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusonline clearfix"><div class="agentstatus"><div class="statusbox"><i class="material-icons">all_out</i></div></div><div class="agentinfo"><p class="ext">' + ext + '</p><p>' + name + '</p></div></div></div>');
		});
		var req_livedata = JSON.stringify({tenantid:tenid,req:"livedata",tid:"100"});
		socket.send(req_livedata);
	}else if(data.abandoned != undefined){
		var result = $.grep(qgraph,function(e){
			if(e.queue == data.queue){
				return true;
			}else{
				return false;
			}
			return false;
		});

		if(result.length == 0){
			qgraph.push(data);
		}else{
			result[0].abandoned = data.abandoned;
			result[0].handled = data.handled;
		}
		chart.load({
			json:qgraph,
			keys:{
				x:'queue',
				value:['abandoned','handled']
			}
		});
	}else{
		if(data.msg_type == "queueAgent_event_ready"){
			var result = $.grep(queues,function(e){
				if(e.agent == data.agent){
					if(e.queue = data.queue){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
				return false;
			});
			if(result.length == 0){
				queues.push({queue:data.queue,agent:data.agent,ready:data.ready});
			}else{
				result[0].ready = data.ready;
			}
			if(cur_queue == data.queue){
				drawforce(data.queue);
			}
		}
	}
}

function agentsort(){
	var alist = $('#agentslist');
	var aboxs = alist.children('.agentbox');

	aboxs.sort(function(a,b){
		var id = a.getAttribute('id');
		if($('#'+id).find('.statusboxinactive').length == 1){
			return 1;
		}else{
			return -1;
		}
		return 0;
	});

	aboxs.detach().appendTo(alist);
}

function drawforce(queue){
	cur_queue = queue;
	$('#agentqueue').html('');
	var width = $('#agentqueue').width();
	var height = $('#agentqueue').height();
	var color = d3.scale.category10();
	nodes = [];
	links = [];

	var force = d3.layout.force()
		.nodes(nodes)
		.links(links)
		.charge(-400)
		.linkDistance(120)
		.size([width,height])
		.on("tick",tick);

	var svg = d3.select("#agentqueue").append("svg")
		.attr("width",width)
		.attr("height",height);

	var node = svg.selectAll(".node"),
		link = svg.selectAll(".link");

	function render() {
	  link = link.data(force.links(), function(d) { return d.source.id + "-" + d.target.id; });
	  link.enter().insert("line", ".node").attr("class", function(d) { return "qlink ready" + d.target.ready; });
	  link.exit().remove();

	  node = node.data(force.nodes(), function(d) { return d.id;});
	  var t = node.enter().append('g').classed('node',true);
	  t.append("circle").attr("class", function(d) { return "node ready" + d.ready; }).attr("r", 20).append("title").text(function(d){return d.id});
	  t.append("text").attr("class","nodetext").attr('x',-10).attr("dy",4).text(function(d){return d.id});
	  node.exit().remove();

	  force.start();
	}

	function tick() {
		//node.attr("cx", function(d) { return d.x; }).attr("cy", function(d) { return d.y; })

		node.attr("transform", function(d) { return 'translate(' + [d.x, d.y] + ')'; })

		link.attr("x1", function(d) { return d.source.x; })
		  .attr("y1", function(d) { return d.source.y; })
		  .attr("x2", function(d) { return d.target.x; })
		  .attr("y2", function(d) { return d.target.y; });
	}
	console.log(queues);
	mainnode = {id:queue,fixed: true,x: width/2,y: height/2};
	var q = null;

	var result = $.grep(queues,function(e){
		return e.queue == queue;
	});

	$.each(result,function(i,e){
		var cnode = {id:e.agent,ready:e.ready};
		nodes.push(mainnode,cnode);
		links.push({source: mainnode,target:cnode});
	})
	render();
}

var chart = c3.generate({
	bindto: '#bargraph',
	data:{
		json:[],
		type:'bar',
		keys:{
			x:'queue',
			value:['abandoned','handled']
		}
	},
	axis:{
		y:{

		},
		x:{
			type:'category'
		}
	}
});

});
