$(function() {
            "use strict";
            //code added for graphs starts here
            var aqueues = [];
            var agraph = [];
            var agentqueues_live={};
            var graph_scattered_data=[];
            var graph_oncall_data=[];
       
            $("#sleft").click(function () { 
				
				var leftPos = $('#agentslist').scrollLeft();
				$("#agentslist").animate({scrollLeft: leftPos - 200}, 100);
			});

			$("#sright").click(function () { 
				
				var leftPos = $('#agentslist').scrollLeft();
				$("#agentslist").animate({scrollLeft: leftPos + 200}, 100);
			});
			$("#qsleft").click(function () { 
				
				var leftPos = $('#queueslist').scrollLeft();
				//alert(leftPos)
				$("#queueslist").animate({scrollLeft: leftPos - 200}, 100);
			});

			$("#qsright").click(function () { 
				
				var leftPos = $('#queueslist').scrollLeft();
				//alert(leftPos)
				$("#queueslist").animate({scrollLeft: leftPos + 200}, 100);
			});

			$('#agentslist').on('click','.agentbox',function(){
				$("div#nodegraphstatus").html("Agent Node Graph for "+$(this).find('.ext').text());
				$(".agentbox").removeClass('a-active');
				$(this).addClass('a-active');
				drawforce($(this).find('.ext').text(),2);
			});
			
			$('#queueslist').on('click','.queuebox',function(){
				$("div#nodegraphstatus").html("Queue Node Graph for "+$(this).find('.ext').text());
				$(".queuebox").removeClass('a-active');
				$(this).addClass('a-active');
				drawforce($(this).find('.ext').text(),1);
			});

            
            //code added for graph ends here
            
            var content = $('#content');
            var socket = atmosphere;
            var subsocket;
            var transport = 'websocket';
            var url="http://"+document.domain+":8080/"
            //var _clientId='362781'
	        var _clientId=getCookie("_id");
            var agent_av=[];
            
           
            //var connectionUrl = "http://10.226.14.80:8081/middleware-5-0_2.11-1.2/" + 'domain/'+ _clientId
		var connectionUrl = url+ 'domain/'+ _clientId
            console.log("About to connect to: " + connectionUrl)

            var request = {
                url: connectionUrl,
                contentType : "application/json",
                logLevel: "debug",
                trackMessageSize: true,
                transport : transport,
                reconnectInterval : 5000
            };

            request.onOpen = function(response) {
                console.log("Atmosphere connected with: " + response.transport)
                transport = response.transport;

                // Carry the UUID. This is required if you want to call subscribe(request) again.
                request.uuid = response.request.uuid;
                
		var initReq=atmosphere.util.stringifyJSON({ transactionId: _clientId, process:"request", command: "live_init", commandType:"initialize",message: ""})
                subsocket.push(initReq);
                	var staticReq=atmosphere.util.stringifyJSON({ transactionId: _clientId, process:"request", command: "static_init", commandType:"initialize",message: ""})
                                subsocket.push(staticReq);
		setTimeout(function() { liveData(subsocket); }, 10000);
            };

             request.onClientTimeout = function(r) {
                    content.html($('<p>', { text: 'Client closed the connection after a timeout. Reconnecting in ' + request.reconnectInterval }));
                    subSocket.push(atmosphere.util.stringifyJSON({ author: author, message: 'is inactive and closed the connection. Will reconnect in ' + request.reconnectInterval }));
                    setTimeout(function (){
                        subSocket = socket.subscribe(request);
                    }, request.reconnectInterval);
                };

            request.onMessage = function (response) {
                var msg=response.responseBody                 
                try {
                var jsonData= atmosphere.util.parseJSON(msg);
                var _arrMsg=JSON.stringify(jsonData.message);
                var _arrJSON=JSON.parse(JSON.stringify(jsonData.message));
               
               
                console.info("RECV "+jsonData.command+"  "+jsonData.commandType+" "+_arrMsg);
              
                 switch(jsonData.command){
                        case "live_snapshot":
                        var _commandType=jsonData.commandType.split(":");
                         switch(_commandType[0]){
                         case "cfi.agent.event":
                         
                         if(_commandType[1] == "create"){
							var alist = $('#agentslist');
							alist.html('');							
							$.each(_arrJSON,function(i,e){
								
								var ext = e.extension;
								var name = e.fullname;
								var queues=e.queues;
								var on_a_call=e.on_a_call;
								var ready=e.ready;
								var reason='';
								if(e.reason=="Server Internal Error") {
									if(ready==1) {
										reason="Available";
									} else {
										reason="Unavailable";
									}
								} else {									
									if(ready==1) {
										reason="Available";
									} else {
										reason=e.reason;
									}
								}								
								
								if(ready==1) {									
									agent_av.push(ext);
									alist.append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusonline clearfix"><div class="agentstatus"><div class="statusbox statusboxactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" >' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span id="'+ext+'" style="background:#d1e6ee;">'+queues+'</span></div><div class="third_row"><span class="text-center">'+reason+'</span><span class="text-center">00:00:12</span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#">Coach</a></span><span class="glyphicon glyphicon-off pull-right text-center red"></span></div></div>');
								}
								if(ready==0) {
									alist.append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusinactive clearfix"><div class="agentstatus"><div class="statusbox statusboxinactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" >' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span id="'+ext+'" style="background:#d1e6ee;">'+queues+'</span></div><div class="third_row"><span class="text-center">'+reason+'</span><span class="text-center">00:00:12</span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#">Coach</a></span><span class="glyphicon glyphicon-off pull-right text-center red"></span></div></div>');
								}								
								
								if(e.queues != undefined){
									aqueues.push([e.agent_name,e.queues]);
								}
								
						
							});
							var sendData=atmosphere.util.stringifyJSON({ transactionId: _clientId, process:"request", command: "live_feed", commandType:"agent_event", message: ""});
							subsocket.push(sendData);
							
						}
						
						
                         break;
                         case "cfi.queue.event":
                           var sendQueueData=atmosphere.util.stringifyJSON({ transactionId: _clientId, process:"request", command: "live_feed", commandType:"queue_event", message: ""});
						   subsocket.push(sendQueueData);
						                        
                         if(_commandType[1] == "create"){
							var qlist = $('#queueslist');
							qlist.html('');
							
							$.each(_arrJSON,function(i,e){
					
								var ext = e.extension;
								var name = e.extname;
								var sessions=e.sessions;
								var agents_available=e.available_count
								
								if(agents_available>0) {									
									graph_oncall_data.push({ 
											"ext" : ext,
											"frequency"  : sessions											
									});
								
									if(sessions>0) {
										qlist.append('<div class="queuebox" id="' + ext + '"><div class="roundcard statusbusy clearfix"><div class="agentstatus"><div class="statusbox statusboxactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" >' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span id="'+ext+'" style="background:#d1e6ee;">'+sessions+'  connected</span></div><div class="third_row"><span class="text-center"></span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#"></a></span></div></div>');
									} else {
										qlist.append('<div class="queuebox" id="' + ext + '"><div class="roundcard statusonline clearfix"><div class="agentstatus"><div class="statusbox statusboxactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" >' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span id="'+ext+'" style="background:#d1e6ee;">'+sessions+'  connected</span></div><div class="third_row"><span class="text-center"></span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#"></a></span></div></div>');
									}
								}
								
								if(e.agents != undefined){
									aqueues.push([e.extension,e.agents]);
								}
								
						
							}); 
							
							draw_sg_onacall(graph_oncall_data)
							
						 } else	 if(_commandType[1] == "graph"){
							 
							
							// console.log("RECV GRAPH "+_arrJSON.toSource());
							 draw_sg1(_arrJSON);
							 
						 }
						
						
                         break;
                         
                         case "cfi.agent.stats":
                         case "cfi.queue.stats":
                         }
                        break;
                        case "live_fed":
                       
                            var _commandType=jsonData.commandType.split(":");
                           
                            if(_commandType[0]=="cfi.agent.event") {
								if(_commandType[1]=="external_busy_update") {
									
									if(jsonData.message.external_busy==1 && jsonData.message.ready==1 ) {
									
										$('#'+jsonData.message.agent_name).find('.roundcard').removeClass('statusonline');
										$('#'+jsonData.message.agent_name).find('.roundcard').removeClass('statusinactive');
										$('#'+jsonData.message.agent_name).find('.roundcard').addClass('statusbusy');
									}else if(jsonData.message.external_busy == 0  && jsonData.message.ready==1){
										$('#'+jsonData.message.agent_name).find('.roundcard').removeClass('statusbusy');
										$('#'+jsonData.message.agent_name).find('.roundcard').removeClass('statusinactive');
										$('#'+jsonData.message.agent_name).find('.roundcard').addClass('statusonline');
									}
									
								
									
								}
						    
								
								
								 if(_commandType[1]=="agent_ready") {
									 
									if(jsonData.message.ready==1) {
										if ($.inArray(jsonData.message.agent_name, agent_av) == -1)
										{
											agent_av.push(jsonData.message.agent_name);
										}
									
										$('#'+jsonData.message.agent_name).find('.roundcard').removeClass('statusinactive');
										$('#'+jsonData.message.agent_name).find('.roundcard').addClass('statusonline');
										$('#'+jsonData.message.agent_name).find('.statusbox').removeClass('statusboxinactive');
									}
								 }
								 
								if(_commandType[1]=="agent_away") {	
									
									 if(jsonData.message.ready == 0){
										 
										if ($.inArray(jsonData.message.agent_name, agent_av) != -1)
										{   agent_av.splice($.inArray(jsonData.message.agent_name, agent_av),1);
											
										}	 
										$('#'+jsonData.message.agent_name).find('.roundcard').addClass('statusinactive');
										$('#'+jsonData.message.agent_name).find('.statusbox').addClass('statusboxinactive');
									}
									
								}
						    
								if(_commandType[1]=="accept_call") {	
									 if(jsonData.message.on_a_call == 1){
										$('#agentslist').find('span#'+jsonData.message.agent_name).html(jsonData.message.queue_name +":"+jsonData.message.call_source);
										
									}
									
								}
								if(_commandType[1] == "logout"){
									$('div #agentslist div#'+jsonData.message.extension).remove();
									
								}
								if(_commandType[1] == "login"){
									console.log("Message login ",jsonData.message);
								    var ext = jsonData.message.extension;
									var name = jsonData.message.fullname;
									var queues=jsonData.message.queues;									
									var ready=jsonData.message.ready;
									var reason='';
									if(jsonData.message.reason=="Server Internal Error") {
										if(ready==1) {
											reason="Available";
										} else {
											reason="Unavailable";
										}
									} else {
										
										if(ready==1) {
											reason="Available";
										} else {
											reason=jsonData.message.reason;
										}
									}								
									
								if(ready==1) {
									
									agent_av.push(ext);
									$('#agentslist').append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusonline clearfix"><div class="agentstatus"><div class="statusbox statusboxactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" >' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span id="'+ext+'" style="background:#d1e6ee;">'+queues+'</span></div><div class="third_row"><span class="text-center">'+reason+'</span><span class="text-center">00:00:12</span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#">Coach</a></span><span class="glyphicon glyphicon-off pull-right text-center red"></span></div></div>');
									
								}
								if(ready==0) {
									
									$('#agentslist').append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusinactive clearfix"><div class="agentstatus"><div class="statusbox statusboxinactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" >' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span id="'+ext+'" style="background:#d1e6ee;">'+queues+'</span></div><div class="third_row"><span class="text-center">'+reason+'</span><span class="text-center">00:00:12</span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#">Coach</a></span><span class="glyphicon glyphicon-off pull-right text-center red"></span></div></div>');
									
								}
								
								
								if(queues != undefined){
									
									aqueues.push([ext,queues]);
								}
							}
							
						 }
						 
						if(_commandType[0]=="cfi.queue.event") { 
							 
							 
							 if(_commandType[1]=="join" || _commandType[1]=="leave" ) {	
								
									 if(jsonData.message.sessions !=undefined){	
										 
										 	$.each(graph_oncall_data, function(i,obj) {
												
											  if (obj.ext ===jsonData.message.extension) {
												  
													graph_oncall_data[i].frequency=jsonData.message.sessions;
												 
											  } 
											});  
											
										 
										if(jsonData.message.sessions >0){											
												 							
											$('div#queueslist  #'+jsonData.message.extension).find('.roundcard').addClass('statusbusy');
											$('#queueslist').find('span#'+jsonData.message.extension).html(jsonData.message.sessions +"  connected");
										} else {
											$('div#queueslist  #'+jsonData.message.extension).find('.roundcard').removeClass('statusbusy');
											$('div#queueslist  #'+jsonData.message.extension).find('.roundcard').addClass('statusonline');
											$('#queueslist').find('span#'+jsonData.message.extension).html(jsonData.message.sessions +"  connected");
											
										}
										draw_sg_onacall(graph_oncall_data)
									}
									
									
									
								}
								
								if(_commandType[1]=="delete") {
									
									//alert(jsonData.toSource());
									var ext = jsonData.message.extension;
									var name = jsonData.message.extname;								
									var agents_available=jsonData.message.available_count
									
									if(agents_available<1) {									
										
										$('div #queueslist div#'+jsonData.message.extension).remove();
									
									}
								
							
									
								}
								
								if(_commandType[1]=="create") {
									
									//alert(jsonData.toSource());
									var ext = jsonData.message.extension;
									var name = jsonData.message.extname;								
									var agents_available=jsonData.message.available_count
									var sessions=0;
									
									if(agents_available>0) {									
										
										$('div #queueslist').append('<div class="queuebox" id="' + ext + '"><div class="roundcard statusonline clearfix"><div class="agentstatus"><div class="statusbox statusboxactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" >' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span id="'+ext+'" style="background:#d1e6ee;">'+sessions+'  connected</span></div><div class="third_row"><span class="text-center"></span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#"></a></span></div></div>');
									
									}  
									
							
									
								}
						}
                        break;
                       case "static_snapshot":
                              switch(jsonData.commandType){
                              	case 'queued_calls':
                              	  $.event.trigger({
                                            type:"reportBlock",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                              	break;
                              	case 'acds':
                              	  $.event.trigger({
                                            type:"acdsBlock",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                              	break;
                              	default:
                              	break;
                              }
                        break;
                        case "static_disposition":
                        		 console.info("Static Diposition");
                        		switch(jsonData.commandType){
                        			case 'queued_calls':
                        			 console.info("Static Diposition QUEUE Calls");
                        			$.event.trigger({
                                            type:"queueDisposition",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                        			break;
                        			case 'abandoned_calls':
                        			console.info("Static Diposition Abandoned Calls");
                        			$.event.trigger({
                                            type:"abandonedDisposition",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
					break;
						case 'abnd_reason_calls':
                        			console.info("Static Diposition Abandoned REASON Calls");
                        			$.event.trigger({
                                            type:"aggreagteByReasonAbandoned",
                                            tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                        			break;
                        			default:
                        			break;
                        		}
                        break;
                 }
                 } catch (e) {
                            console.log('This doesn\'t look like a valid JSON: ', msg);
                            return;
                 }
            };

             request.onReopen = function(response) {
                  content.html($('<p>', { text: 'Atmosphere re-connected using ' + response.transport }));
                };

                // For demonstration of how you can customize the fallbackTransport using the onTransportFailure function
             request.onTransportFailure = function(errorMsg, request) {
                    atmosphere.util.info(errorMsg);
                    request.fallbackTransport = "long-polling";
                    header.html($('<h3>', { text: 'Atmosphere Chat. Default transport is WebSocket, fallback is ' + request.fallbackTransport }));
             };

                request.onClose = function(response) {
                    content.html($('<p>', { text: 'Server closed the connection after a timeout' }));
                    if (subsocket) {
                        subsocket.push(atmosphere.util.stringifyJSON({ author: author, message: 'disconnecting' }));
                    }
                };

                request.onError = function(response) {
                    content.html($('<p>', { text: 'Sorry, but there\'s some problem with your '
                        + 'socket or the server is down' }));
                    logged = false;
                };

                request.onReconnect = function(request, response) {
                    content.html($('<p>', { text: 'Connection lost, trying to reconnect. Trying to reconnect ' + request.reconnectInterval}));

                };


            subsocket = socket.subscribe(request);

           
            
 
			function agentsort(){
				var alist = $('#agentslist');
				var aboxs = alist.children('.agentbox');

				aboxs.sort(function(a,b){
					var id = a.getAttribute('id');
					if($('#'+id).find('.statusboxinactive').length == 1){
						return 1;
					}else{
						return -1;
					}
					return 0;
				});

				aboxs.detach().appendTo(alist);
			}

			function drawforce(agent, type){
				
				$('#agentqueue').html('');
				var width = $('#agentqueue').width();
				var height = $('#agentqueue').height();
				var color = d3.scale.category10();
				var nodes = [];
				var links = [];
				var mainnode= {}

				var force = d3.layout.force()
					.nodes(nodes)
					.links(links)
					.charge(-100)
					.linkDistance(120)
					.size([width,height])
					.on("tick",tick);

				var svg = d3.select("#agentqueue").append("svg")
					.attr("width",width)
					.attr("height",height);

				var node = svg.selectAll(".node"),
					link = svg.selectAll(".link");

				function render() {
				  link = link.data(force.links(), function(d) { return d.source.id + "-" + d.target.id; });
				  link.enter().insert("line", ".node").attr("class", "link");
				  link.exit().remove();

				  node = node.data(force.nodes(), function(d) { return d.id;});
				  var t = node.enter().append('g').classed('node',true);
				  t.append("circle").attr("class", function(d) { return "node " + d.id; }).attr("r", 20).append("title").text(function(d){return d.id});
				  t.append("text").attr("class","nodetext").attr('x',-10).attr("dy",4).text(function(d){return d.id});
				  node.exit().remove();

				  force.start();
				}

				function tick() {
					//node.attr("cx", function(d) { return d.x; }).attr("cy", function(d) { return d.y; })

					node.attr("transform", function(d) { return 'translate(' + [d.x, d.y] + ')'; })

					link.attr("x1", function(d) { return d.source.x; })
					  .attr("y1", function(d) { return d.source.y; })
					  .attr("x2", function(d) { return d.target.x; })
					  .attr("y2", function(d) { return d.target.y; });
				}

				mainnode = {id:agent,fixed: true,x: width/2,y: height/2};
				var q = null;
				
				for(var i = 0; i < aqueues.length; i++) {
					
					if(aqueues[i][0] == agent){
						q = aqueues[i][1].split(",");
					}
				}
				$.each(q,function(i,e){
					e=e.replace('[','');
					e=e.replace(']','');
					var m=$.trim(e);
					
					//queue with avilable agent nodes
					    if(type==1) {
					
							if ($.inArray(m, agent_av) != -1) { 	
								
								var cnode = {id:e};
								nodes.push(mainnode,cnode);
								links.push({source: mainnode,target:cnode});
							}
						
					    }
					    
					    //agents with avilable queue nodes
					    if(type==2) {
					
								var cnode = {id:e};
								nodes.push(mainnode,cnode);
								links.push({source: mainnode,target:cnode});
						
						
					    }
				})
				render();
			}

	
			//Collapse menu
   

$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
     $("#menu-toggle-2").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled-2");
      //  $('#menu ul').hide();
    });
/*
     function initMenu() {
      $('#menu ul').hide();
      //$('#menu ul').children('.current').parent().show();
	  // 
	   	  $('#menu ul').show();
	  
	  
      $('#menu li a').click(
        function() {
          var checkElement = $(this).next();
          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
            }
          if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
            return false;
            }
			
			
			
          }
        );
      }
	  	  
		
	  
    $(document).ready(function() {initMenu();});
	
	*/
	
	$(document).ready(function () {	
	
	$('#menu li ul ').hide();
		
  $('#menu > li > a').click(function(){
	 
    if ($(this).attr('class') != 'active'){
      $('#menu li ul').slideUp();
      $(this).next().slideToggle();
      $('#menu li a').removeClass('active');
      $(this).addClass('active');	  
      }
  });
  
});
		
        });

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
 }
function liveData(socket){
var sendData=atmosphere.util.stringifyJSON({ transactionId: "", process:"request", command: "live_init", commandType:"agent_event", message: ""})
                socket.push(sendData);
                 console.log("Message sent E ",sendData);
            var sendQueueData=atmosphere.util.stringifyJSON({ transactionId: "", process:"request", command: "live_init", commandType:"queue_event", message: ""})
                socket.push(sendQueueData);
                 console.log("Message sent Q ",sendQueueData);
                  var sendQueueGraph=atmosphere.util.stringifyJSON({ transactionId: "", process:"request", command: "live_init", commandType:"queue_graph", message: ""})
                   socket.push(sendQueueGraph);
                  						 $.event.trigger({
                                                             type:"socketOnOpen",
                                                              _subsocket:socket
                                                      });
}

