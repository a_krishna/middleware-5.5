$(document).ready(function() {
var tenid = "362781";
var ip = "10.226.14.80";
var port = "7575";
//var ip = "127.0.0.1";
//var port = "8080";
var aqueues = [];
var agraph = [];

// Events
$("#sleft").click(function () { 
	var leftPos = $('#agentslist').scrollLeft();
	$("#agentslist").animate({scrollLeft: leftPos - 200}, 100);
});

$("#sright").click(function () { 
	var leftPos = $('#agentslist').scrollLeft();
	$("#agentslist").animate({scrollLeft: leftPos + 200}, 100);
});

$('#agentslist').on('click','.agentbox',function(){
	$(".agentbox").removeClass('a-active');
	$(this).addClass('a-active');
	drawforce($(this).find('.ext').text());
});

// Web Sockets
var socket = null;
socket = new WebSocket('ws://' + ip + ':' + port + '/sock');

socket.onerror = function(event){
	//noty({text: 'Unable to connect!',layout:'bottomRight',theme: 'relax',type:'error'});
}

socket.onclose = function(event){
	//noty({text: 'Disconnected!',layout:'bottomRight',theme: 'relax',type:'error'});
}

socket.onopen = function(event){
	var req_agents = JSON.stringify({tenantid:tenid,req:"agents",tid:"100"});
	socket.send(req_agents);
	//noty({text: 'Connected!',layout:'bottomRight',theme: 'relax',type:'information',timeout:10000});
}

socket.onmessage = function(e){
	
	var data = jQuery.parseJSON(e.data);	
	//alert(data.toSource());
	console.log(data);
	if(data[0] != undefined){
		var alist = $('#agentslist');
		alist.html('');
		$.each(data[0].agents,function(i,e){
			var ext = e.number;
			var name = e.name;
			alist.append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusinactive clearfix"><div class="agentstatus"><div class="statusbox statusboxinactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext">' + ext + '</div><div class="ext_2">' + name + '</div></div></div><div class="second_row text-center"><span style="background:#d1e6ee;">501,502,503,504</span></div><div class="third_row"><span class="text-center">Available</span><span class="text-center">00:00:12</span></div> <div class="bottom_row"><span class="pull-left text-center"><a href="#">Coach</a></span><span class="glyphicon glyphicon-off pull-right text-center red" style="padding:2px;"></span></div></div>');
			//alist.append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusinactive clearfix"><div class="agentstatus"><div class="statusbox statusboxinactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><div class="ext" style="width:50%; float:left;">' + ext + '</div><div style="width:50%;float:right;">' + name + '</div></div></div></div>');
			//alist.append('<div class="agentbox" id="' + ext + '"><div class="roundcard statusinactive clearfix"><div class="agentstatus"><div class="statusbox statusboxinactive"><i class="material-icons">call</i></div></div><div class="agentinfo"><p class="ext">' + ext + '</p><p>' + name + '</p></div></div></div>');
		});
		var req_livedata = JSON.stringify({tenantid:tenid,req:"livedata",tid:"100"});
		socket.send(req_livedata);
	}else if(data.connected != undefined){
		var result = $.grep(agraph,function(e){
			if(e.agent == data.agent){
				return true;
			}else{
				return false;
			}
			return false;
		});

		if(result.length == 0){
			agraph.push(data);
		}else{
			result[0].abandoned = data.abandoned;
			result[0].handled = data.handled;
		}
		chart.load({
			json:agraph,
			keys:{
				x:'agent',
				value:['connected','handled']
			}
		});
	}else{
		if(data.msg_type == "agent_event_ready"){
			console.log(data);
			if(data.ready == 1){
				$('#'+data.agent).find('.roundcard').removeClass('statusinactive');
				$('#'+data.agent).find('.roundcard').addClass('statusonline');
				$('#'+data.agent).find('.statusbox').removeClass('statusboxinactive');
			}else if(data.ready == 0){
				$('#'+data.agent).find('.roundcard').addClass('statusinactive');
				$('#'+data.agent).find('.statusbox').addClass('statusboxinactive');
			}
		}
		if(data.msg_type == "agent_event_hook"){
			console.log(data);
			if(data.hook == 1){
				$('#'+data.agent).find('.roundcard').removeClass('statusonline');
				$('#'+data.agent).find('.roundcard').removeClass('statusinactive');
				$('#'+data.agent).find('.roundcard').addClass('statusbusy');
			}else if(data.hook == 0){
				$('#'+data.agent).find('.roundcard').removeClass('statusbusy');
				$('#'+data.agent).find('.roundcard').removeClass('statusinactive');
				$('#'+data.agent).find('.roundcard').addClass('statusonline');
			}
		}
		if(data.queues != undefined){
			aqueues.push([data.agent,data.queues]);
		}
		//agentsort();
	}
}

function agentsort(){
	var alist = $('#agentslist');
	var aboxs = alist.children('.agentbox');

	aboxs.sort(function(a,b){
		var id = a.getAttribute('id');
		if($('#'+id).find('.statusboxinactive').length == 1){
			return 1;
		}else{
			return -1;
		}
		return 0;
	});

	aboxs.detach().appendTo(alist);
}

function drawforce(agent){
	$('#agentqueue').html('');
	var width = $('#agentqueue').width();
	var height = $('#agentqueue').height();
	var color = d3.scale.category10();
	nodes = [];
	links = [];

	var force = d3.layout.force()
		.nodes(nodes)
		.links(links)
		.charge(-100)
		.linkDistance(120)
		.size([width,height])
		.on("tick",tick);

	var svg = d3.select("#agentqueue").append("svg")
		.attr("width",width)
		.attr("height",height);

	var node = svg.selectAll(".node"),
		link = svg.selectAll(".link");

	function render() {
	  link = link.data(force.links(), function(d) { return d.source.id + "-" + d.target.id; });
	  link.enter().insert("line", ".node").attr("class", "link");
	  link.exit().remove();

	  node = node.data(force.nodes(), function(d) { return d.id;});
	  var t = node.enter().append('g').classed('node',true);
	  t.append("circle").attr("class", function(d) { return "node " + d.id; }).attr("r", 20).append("title").text(function(d){return d.id});
	  t.append("text").attr("class","nodetext").attr('x',-10).attr("dy",4).text(function(d){return d.id});
	  node.exit().remove();

	  force.start();
	}

	function tick() {
		//node.attr("cx", function(d) { return d.x; }).attr("cy", function(d) { return d.y; })

		node.attr("transform", function(d) { return 'translate(' + [d.x, d.y] + ')'; })

		link.attr("x1", function(d) { return d.source.x; })
		  .attr("y1", function(d) { return d.source.y; })
		  .attr("x2", function(d) { return d.target.x; })
		  .attr("y2", function(d) { return d.target.y; });
	}

	mainnode = {id:agent,fixed: true,x: width/2,y: height/2};
	var q = null;
	for(var i = 0; i < aqueues.length; i++) {
		if(aqueues[i][0] == agent){
			q = aqueues[i][1].split(",");
		}
	}
	$.each(q,function(i,e){
		var cnode = {id:e};
		nodes.push(mainnode,cnode);
		links.push({source: mainnode,target:cnode});
	})
	render();
}

var chart = c3.generate({
	bindto: '#bargraph',
	data:{
		json:[],
		type:'bar',
		keys:{
			x:'agent',
			value:['connected','handled']
		}
	},
	axis:{
		x:{
			type:'category'
		},
		y:{
			tick:{
				fotmat:d3.format('.0f')
			}
		}
	}
});

//Collapse menu



$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
     $("#menu-toggle-2").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled-2");
      //  $('#menu ul').hide();
    });
/*
     function initMenu() {
      $('#menu ul').hide();
      //$('#menu ul').children('.current').parent().show();
	  // 
	   	  $('#menu ul').show();
	  
	  
      $('#menu li a').click(
        function() {
          var checkElement = $(this).next();
          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
            }
          if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
            return false;
            }
			
			
			
          }
        );
      }
	  	  
		
	  
    $(document).ready(function() {initMenu();});
	
	*/
	
	$(document).ready(function () {	
	
	$('#menu li ul ').hide();
		
  $('#menu > li > a').click(function(){
	 
    if ($(this).attr('class') != 'active'){
      $('#menu li ul').slideUp();
      $(this).next().slideToggle();
      $('#menu li a').removeClass('active');
      $(this).addClass('active');	  
      }
  });
  
});
	

});

