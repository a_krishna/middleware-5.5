$(function() {
            "use strict";
             var content = $('#content');
            var socket = atmosphere;
            var subsocket;
            var transport = 'websocket';
            var dataURLLOC=document.location.toString().split("?")
            var _clientId='362781'
            var agent_av=[];
            
           
            var connectionUrl = "http://10.226.14.80:8081/middleware-5-0_2.11-1.2/" + 'domain/'+ _clientId
            console.log("About to connect to: " + connectionUrl)

            var request = {
                url: connectionUrl,
                contentType : "application/json",
                logLevel: "debug",
                trackMessageSize: true,
                transport : transport,
                reconnectInterval : 5000
            };

            request.onOpen = function(response) {
                console.log("Atmosphere connected with: " + response.transport)
                transport = response.transport;

                // Carry the UUID. This is required if you want to call subscribe(request) again.
                request.uuid = response.request.uuid;
						 $.event.trigger({
                                           type:"socketOnOpen",
                                            _subsocket:subsocket
                                    });
            };

             request.onClientTimeout = function(r) {
                    content.html($('<p>', { text: 'Client closed the connection after a timeout. Reconnecting in ' + request.reconnectInterval }));
                    subSocket.push(atmosphere.util.stringifyJSON({ author: author, message: 'is inactive and closed the connection. Will reconnect in ' + request.reconnectInterval }));
                    setTimeout(function (){
                        subSocket = socket.subscribe(request);
                    }, request.reconnectInterval);
                };

            request.onMessage = function (response) {
                var msg=response.responseBody                 
                try {
                var jsonData= atmosphere.util.parseJSON(msg);
                var _arrMsg=JSON.stringify(jsonData.message);
                var _arrJSON=JSON.parse(JSON.stringify(jsonData.message));
               
               
                console.info("RECV "+jsonData.command+"  "+jsonData.commandType+" "+_arrMsg);
              
                 switch(jsonData.command){
                        case "live_snapshot":
                        var _commandType=jsonData.commandType.split(":");
                         switch(_commandType[0]){
                         case "cfi.agent.event":
                          $.event.trigger({
                                           type:"cfiSnapShotAgentEvent",
                                           action:_commandType[1],
					   message:jsonData.message,
					   jsonMsg:_arrJSON
                                    });
			var sendAgentData=atmosphere.util.stringifyJSON({ transactionId: _clientId, process:"request", command: "live_feed", 						commandType:"agent_event", message: ""});
			subsocket.push(sendAgentData);			
                         break;
                         case "cfi.queue.event":
 				$.event.trigger({
                                           type:"cfiSnapShotQueueEvent",
                                           action:_commandType[1],
					   message:jsonData.message,
					   jsonMsg:_arrJSON
                                    });
			var sendQueueData=atmosphere.util.stringifyJSON({ transactionId: _clientId, process:"request", command: "live_feed", 						 commandType:"queue_event", message: ""});
			subsocket.push(sendQueueData);			
                         break;
                         case "cfi.agent.stats":
					$.event.trigger({
                                           type:"cfiSnapShotEventStats",
                                           action:_commandType[1],
					   message:jsonData.message,
					   jsonMsg:_arrJSON
                                    });
			 break;
                         case "cfi.queue.stats":
					$.event.trigger({
                                           type:"cfiSnapShotQueueStats",
                                           action:_commandType[1],
					   message:jsonData.message,
					   jsonMsg:_arrJSON
                                    });
			 break;
                         }
                        break;
                        case "live_fed":
                            var _commandType=jsonData.commandType.split(":");
                           	switch(_commandType[0]){
				case "cfi.agent.event":
				$.event.trigger({
                                           type:"cfiFedAgentEvent",
                                           action:_commandType[1],
					   message:jsonData.message,
					   jsonMsg:_arrJSON
                                    });
				break;
				case "cfi.queue.event":
					$.event.trigger({
                                           type:"cfiFedQueueEvent",
                                           action:_commandType[1],
					   message:jsonData.message,
					   jsonMsg:_arrJSON
                                    });
				break;
				default:
				break;
				}
                        break;
                       case "static_snapshot":
                              switch(jsonData.commandType){
                              	case 'queued_calls':
                              	  $.event.trigger({
                                            type:"reportBlock",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                              	break;
                              	case 'acds':
                              	  $.event.trigger({
                                            type:"acdsBlock",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                              	break;
                              	default:
                              	break;
                              }
                        break;
                        case "static_disposition":
                        		 console.info("Static Diposition");
                        		switch(jsonData.commandType){
                        			case 'queued_calls':
                        			 console.info("Static Diposition QUEUE Calls");
                        			$.event.trigger({
                                            type:"queueDisposition",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                        			break;
                        			case 'abandoned_calls':
                        			console.info("Static Diposition Abandoned Calls");
                        			$.event.trigger({
                                            type:"abandonedDisposition",
					    tId:jsonData.transactionId,
                                            msg_type: jsonData.commandType,
                                            message:jsonData.message,
                                            _subsocket:subsocket
                                        });
                        			default:
                        			break;
                        		}
                        break;
                 }
                 } catch (e) {
                            console.log('This doesn\'t look like a valid JSON: ', msg);
                            return;
                 }
            };

             request.onReopen = function(response) {
                  content.html($('<p>', { text: 'Atmosphere re-connected using ' + response.transport }));
                };

                // For demonstration of how you can customize the fallbackTransport using the onTransportFailure function
             request.onTransportFailure = function(errorMsg, request) {
                    atmosphere.util.info(errorMsg);
                    request.fallbackTransport = "long-polling";
                    header.html($('<h3>', { text: 'Atmosphere Chat. Default transport is WebSocket, fallback is ' + request.fallbackTransport }));
             };

                request.onClose = function(response) {
                    content.html($('<p>', { text: 'Server closed the connection after a timeout' }));
                    if (subsocket) {
                        subsocket.push(atmosphere.util.stringifyJSON({ author: author, message: 'disconnecting' }));
                    }
                };

                request.onError = function(response) {
                    content.html($('<p>', { text: 'Sorry, but there\'s some problem with your '
                        + 'socket or the server is down' }));
                    logged = false;
                };

                request.onReconnect = function(request, response) {
                    content.html($('<p>', { text: 'Connection lost, trying to reconnect. Trying to reconnect ' + request.reconnectInterval}));

                };


            subsocket = socket.subscribe(request);
		
        });
