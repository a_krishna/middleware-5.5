package com.cfi.middleware.model

import java.lang.reflect.Type
import javax.inject.Inject

import com.fasterxml.jackson.annotation.{JsonAutoDetect, PropertyAccessor}
import com.fasterxml.jackson.databind.ObjectMapper
import org.atmosphere.config.managed.{Decoder, Encoder}
import org.atmosphere.cpr.AtmosphereConfig
import org.atmosphere.inject.Injectable

/**
  * Created by AravindKrishna on 5/12/15.
  */

class ObjectMapperInjectable extends Injectable[ObjectMapper] {
  val mapper: ObjectMapper = new ObjectMapper()
  mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)

  override def supportedType(t: Type): Boolean = (t.isInstanceOf[Class[_]]) && (classOf[ObjectMapper] == t.asInstanceOf[Class[_]])


  override def injectable(atmosphereConfig: AtmosphereConfig): ObjectMapper = mapper
}

class JacksonEncoder extends Encoder[Transaction, String] {
  @Inject var mapper: ObjectMapper = _

  override def encode(u: Transaction): String = mapper.writeValueAsString(u)
}

class JacksonDecoder extends Decoder[String, Transaction] {
  @Inject var mapper: ObjectMapper = _

  override def decode(u: String): Transaction = mapper.readValue(u, classOf[Transaction])
}

case class DataMart(count:Int,handled:Int,abandoned:Int,talk_duration:Int,client_id:Long,time:String,entry_time:String,exit_time:String,answer_delay:Int,abandon_time:Int,total_duration:Int,date:String,service_level_sec:Int,transfer:Short,wrapuptime:Int,queue_id:Long,extname:String,extnum:Int,acdtype:String,service_level:Int)

case class DataMartAbandoned(begin:String, entry:String, queueName:String, callSource:String, queueNum:Int, queueId:Int,totalTime:Int, complete:String, abandonReason:String, abandonTime:String, abandonDuration:Int, redirect:String)

case class Acds(extnum:String,extname:String,id:Int)