package com.cfi.middleware

import java.text.SimpleDateFormat

import akka.actor.{Props, ActorSystem, Actor}
import com.cfi.middleware.message.{FetchAcds, InitDataMart, QueryQueuedCalls}
import com.cfi.middleware.services.datamart.filter.Filter
import com.cfi.middleware.services.datamart.{Tenant, DataMartAccessActor}
import org.joda.time.DateTime

/**
  * Created by AravindKrishna on 14/12/15.
  */
case class DataMartTestActor() extends Actor{
  def receive={
    case _=> println("hello")
  }
}
object DataMartTest extends App with Filter{
  val system=ActorSystem("test-datamart")
  val tempReF=system.actorOf(Props(classOf[DataMartTestActor]))
  val dataMartTest=system.actorOf(Props(new DataMartAccessActor(Tenant.clientId,tempReF)))
  dataMartTest ! InitDataMart
//  dataMartTest ! QueryQueuedCalls("2015-12-11","","383,386,387,388,389","day","3600")
//  dataMartTest ! QueryQueuedCalls("2015-12-11","","383,386,387,388,389","day","900")
//  dataMartTest ! QueryQueuedCalls("2015-12-1","2015-12-31","383,386,387,388,389","month","")
//  dataMartTest ! QueryQueuedCalls("2015-12-1","2015-12-7","383,386,387,388,389","week","")

  dataMartTest ! FetchAcds("13212312")
  println(s"UUID ${uuid}")
  def uuid = java.util.UUID.randomUUID.toString
}
