package com.cfi.middleware.message

import akka.actor.ActorRef
import com.cfi.middleware.model._
import com.cfi.middleware.services.consumer.graph.HandleAgentEvent

import scala.collection.mutable

/**
  * Created by AravindKrishna on 6/12/15.
  */
trait EventRequest

trait LiveData

trait StaticData

trait AMQP

case object InitalizeConusmer extends LiveData with  EventRequest

case class AddActorRef(actorRef:ActorRef) extends LiveData with EventRequest

case object ConsumerShutdown extends LiveData with EventRequest

case object SnapShotAgentEvent extends LiveData with EventRequest

case object SnapShotQueueEvent extends LiveData with EventRequest

case object SnapShotAgentStats extends LiveData with EventRequest

case object SnapShotQueueStats extends LiveData with EventRequest

case object FeedAgentEvent extends LiveData with EventRequest

case object FeedQueueEvent extends LiveData with EventRequest

case object FeedAgentStats extends LiveData with EventRequest

case object FeedQueueStats extends LiveData with EventRequest

case object StopAgentEvent extends LiveData with EventRequest

case object StopQueueEvent extends LiveData with EventRequest

case object StopAgentStats extends LiveData with EventRequest

case object StopQueueStats extends LiveData with EventRequest


case class RecvChunkAgentEvent(headers:java.util.Map[String,Object],routing_key:String,action:String) extends AMQP
case class RecvChunkAgentStats(headers:java.util.Map[String,Object],routing_key:String,action:String) extends AMQP
case class RecvChunkQueueEvent(headers:java.util.Map[String,Object],routing_key:String,action:String) extends AMQP
case class RecvChunkQueueStats(headers:java.util.Map[String,Object],routing_key:String,action:String) extends AMQP


trait EventResponse

case class AgentEventSnapShot(agentEvents: mutable.Set[AgentEvent]) extends LiveData with EventResponse

case class QueueEventSnapShot(queueEvents: mutable.Set[QueueEvent]) extends LiveData with EventResponse

case class AgentStatsSnapShot(agentStats: mutable.Set[AgentStats]) extends LiveData with EventResponse

case class QueueStatsSnapShot(queueStats: mutable.Set[QueueStats]) extends LiveData with EventResponse


case class AgentEventFeed(agentEvent: AgentEvent, action: String) extends LiveData with EventResponse

case class UnAgentEventFeed(objectMap: java.util.Map[String,Object], action: String) extends LiveData with EventResponse

case class QueueEventFeed(queueEvent: QueueEvent, action: String) extends LiveData with EventResponse

case class AgentStatsFeed(agentStat: AgentStats, action: String) extends LiveData with EventResponse

case class QueueStatsFeed(queueStat: QueueStats, action: String) extends LiveData with EventResponse

trait live_graph

case object SnapShotQueueGraph extends live_graph with EventRequest

case class CreateQueueGraph(handleAgentEvent: HandleAgentEvent)  extends live_graph with EventRequest

case class LoginQueueGraph(agentGraph: AgentGraph) extends live_graph with EventRequest

case class LogOutQueueGraph(agentGraph: AgentGraph) extends live_graph with EventRequest

case class QueueGraphSnapShot(queueGraphs:mutable.Set[QueueGraph]) extends live_graph with EventResponse

trait _StaticData

case object InitDataMart extends _StaticData with EventRequest

case class Authentication(userName:String,password:String,tenant:String) extends _StaticData with  EventRequest

case  class FetchAcds(transactionId:String) extends _StaticData with EventRequest

case class QueryQueuedCalls(transactionId:String,startTime:String,endTime:String,acd:String,period:String,timeDelay:String) extends _StaticData with EventRequest

case class QueryQueuedDisposition(transactionId:String,startTime:String,endTime:String,acd:String,reasons:String) extends _StaticData with EventRequest

case class AcdsResponse(transactionId:String,records:Array[java.util.Map[String,Object]]) extends _StaticData with EventResponse

case class QueuedCallsResponse(transactionId:String,records:Array[java.util.Map[String,Object]]) extends _StaticData with EventResponse

case class QueuedCallsDispositionResponse(transactionId:String,records:Array[java.util.Map[String,Object]]) extends _StaticData with EventResponse

case class AbandonedCallsDispositionResponse(transactionId:String,records:Array[java.util.Map[String,Object]]) extends _StaticData with EventResponse

case class AbandonedCallsSecondDispositionResponse(transactionId:String,records:Array[java.util.Map[String,Object]]) extends _StaticData with EventResponse



object LiveState extends Enumeration {
  type STATE = Value
  val DISABLE, STOP, FEED = Value
}
