package com.cfi.middleware.config.akka

import com.cfi.middleware.config.akka.ConfigModule.ConfigProvider
import com.google.inject.{AbstractModule, Provider}
import com.typesafe.config.{Config, ConfigFactory}
import net.codingwell.scalaguice.ScalaModule

/**
  * Created by AravindKrishna on 1/12/15.
  */
object ConfigModule {

  class ConfigProvider extends Provider[Config] {
    override def get() = ConfigFactory.load()
  }

}

class ConfigModule extends AbstractModule with ScalaModule {
  override def configure(): Unit = {
    bind[Config].toProvider[ConfigProvider].asEagerSingleton()
  }
}
