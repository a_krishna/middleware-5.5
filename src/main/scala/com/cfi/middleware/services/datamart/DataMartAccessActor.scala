package com.cfi.middleware.services.datamart

import java.util
import java.util.Calendar

import akka.actor.{ActorLogging, Actor, ActorRef}
import com.cfi.middleware.message._
import com.cfi.middleware.model.{DataMartAbandoned, DataMart}
import com.cfi.middleware.services.datamart.filter.Normalization
import com.cfi.middleware.services.datamart.utils.{DataBaseConfig, DataMartService}
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by AravindKrishna on 9/12/15.
  */
class DataMartAccessActor(clientId:String,actorRef: ActorRef) extends Actor with ActorLogging with DataMartService with DataBaseConfig with Normalization{
  import  driver._

  override def preStart()= {
    db.source.createConnection()
    dbCon.source.createConnection()
  }

  override def postStop()={
    db.source.close()
    dbCon.source.close()
  }

  override def receive: Receive = {
    case InitDataMart=>
      context.system.log.info(s"INITALIZED DATAMART ${db}")
    case QueryQueuedCalls(tId,sT,eT,acd,period,timeDelay)=> period match {
      case "day"=>
        timeDelay.toInt match {
          case 900=>
            val data=Await.result(db.run(dataMartTimeFilter(sT,acd.split(","))),Duration.Inf)
            val _result=calculateForHour(data,minutesFilter,new ArrayBuffer[java.util.Map[String,Object]]())
            actorRef ! QueuedCallsResponse(tId,_result.toArray)
          case 3600=>
            val data=Await.result(db.run(dataMartTimeFilter(sT,acd.split(","))),Duration.Inf)
            val _result= calculateForHour(data,hourFilter,new ArrayBuffer[java.util.Map[String,Object]]())
            actorRef ! QueuedCallsResponse(tId,_result.toArray)
          case _=>
        }
      case "week"=>
        timeDelay.matches("[0-9]+") match {
          case true =>
            val _data=Await.result(db.run(dataMart(dayOfMonth(sT,eT,timeDelay.toInt),eT,acd.split(","))),Duration.Inf)
            val _result=averageOfDayOfWeek(_data,dayOfMonth(sT,eT,timeDelay.toInt))
            actorRef ! QueuedCallsResponse(tId,_result.toArray)
          case false =>  val _data=Await.result(db.run(dataMartDateFilter(sT,eT,acd.split(","))),Duration.Inf)
            val _result=calculateForDays(_data,sT,eT,new ArrayBuffer[util.Map[String, Object]]())
            actorRef ! QueuedCallsResponse(tId,_result.toArray)
        }

      case "month"=>
       timeDelay.matches("[0-9]+") match {
         case true=>val _data=Await.result(db.run(dataMart(dayOfMonth(sT,eT,timeDelay.toInt),eT,acd.split(","))),Duration.Inf)
           val _result=averageOfDayOfWeek(_data,dayOfMonth(sT,eT,timeDelay.toInt))
           actorRef ! QueuedCallsResponse(tId,_result.toArray)
         case false=>val _data=Await.result(db.run(dataMartDateFilter(sT,eT,acd.split(","))),Duration.Inf)
           val _result=calculateForDays(_data,sT,eT,new ArrayBuffer[util.Map[String, Object]]())
           actorRef ! QueuedCallsResponse(tId,_result.toArray)
       }
      case "year"=>
      case _=>
    }
    case FetchAcds(tId) => val _data=Await.result(dbCon.run(fetchAcds(Tenant.clientId,"")),Duration.Inf)
      val resultMap=new Array[java.util.Map[String,Object]](_data.size)
      var count=0
      _data.foreach{z=> val objectMap=new util.HashMap[String,Object]()
      objectMap.put("extnum",z.extnum)
        objectMap.put("extname",z.extname)
        objectMap.put("id",z.id.toString)
        resultMap.update(count,objectMap)
        count+=1
      }
      actorRef ! AcdsResponse(tId,resultMap)
    case QueryQueuedDisposition(tID,sT,eT,acds,reasons)=>
      var _datamart:Vector[DataMart]=null
      var _datamartAdnd:Vector[DataMartAbandoned]=null
      eT.equals("") match {
        case true =>
          _datamart=Await.result(db.run(dataMartTimeFilter(sT,acds.split(","))),Duration.Inf)
           _datamartAdnd=Await.result(db.run(abandonedCallsFilter(sT,sT,acds.split(","),reasons.split(","))),Duration.Inf)
        case false=>
           _datamart=Await.result(db.run(dataMartDateFilter(sT,eT,acds.split(","))),Duration.Inf)
           _datamartAdnd=Await.result(db.run(abandonedCallsFilter(sT,eT,acds.split(","),reasons.split(","))),Duration.Inf)
      }
      context.system.log.debug(s"DISPOSE ${_datamartAdnd}")
      actorRef ! QueuedCallsDispositionResponse(tID,normalizeQueueDisposition(_datamart,acds.split(","),new ArrayBuffer[util.Map[String, Object]]()).toArray)
      actorRef ! AbandonedCallsDispositionResponse(tID,normalizeAbandonedCalls(_datamartAdnd,new ArrayBuffer[util.Map[String, Object]]()).toArray)
      actorRef ! AbandonedCallsSecondDispositionResponse(tID,normalizeSecondLevelAbandonedCalls(_datamartAdnd,acds.split(","),reasons.split(","),new ArrayBuffer[util.Map[String,Object]]()).toArray)
    case _=>
  }
}
