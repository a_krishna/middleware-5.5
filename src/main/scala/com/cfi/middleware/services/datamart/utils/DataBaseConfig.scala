package com.cfi.middleware.services.datamart.utils

/**
  * Created by AravindKrishna on 9/12/15.
  */

trait DataBaseConfig {
  val driver = slick.driver.MySQLDriver

  import driver.api._

  lazy val database = Database.forConfig("datamart")

  lazy val databaseCon = Database.forConfig("context")

  val db = database

  val dbCon = databaseCon

  implicit val session: Session = db.createSession()

  implicit val sessionCon: Session = dbCon.createSession()
}
