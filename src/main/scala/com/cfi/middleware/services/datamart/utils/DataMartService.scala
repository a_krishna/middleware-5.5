package com.cfi.middleware.services.datamart.utils

import com.cfi.middleware.model.{DataMartAbandoned, Acds, DataMart}
import com.cfi.middleware.services.datamart.Tenant
import slick.driver.JdbcProfile
import slick.jdbc.{PositionedParameters, SetParameter, GetResult}
import slick.profile.SqlStreamingAction

/**
  * Created by AravindKrishna on 9/12/15.
  */
trait DataMartService {
  protected val driver:JdbcProfile

  var dataSize=5

  val indices:Int=>(Int,Int)=(z=> (z*dataSize-dataSize,dataSize))



  implicit val getUserResult = GetResult(r => DataMart(r.nextInt(), r.nextInt(), r.nextInt(), r.nextInt(), r.nextLong(), r.nextString(), r.nextString(), r.nextString(), r.nextInt(), r.nextInt(), r.nextInt(), r.nextString(), r.nextInt(), r.nextShort(), r.nextInt(), r.nextLong(), r.nextString(),r.nextInt(), r.nextString(), r.nextInt()))

  implicit val getAcdResult= GetResult(r=> Acds(r.nextString(),r.nextString(),r.nextInt()))

  implicit val getAbandonedResult= GetResult(r=> DataMartAbandoned(r.nextString(),r.nextString(),r.nextString(),r.nextString(),r.nextInt(),r.nextInt(),r.nextInt(),r.nextString(),r.nextString(),r.nextString(),r.nextInt(),r.nextString()))

  import driver.api._
  def getCCParams(cc: AnyRef) =
    (Map[String, Any]() /: cc.getClass.getDeclaredFields) {(a, f) =>
      f.setAccessible(true)
      a + (f.getName -> f.get(cc))
    }

  val dataMart:(Seq[String],String,Seq[String])=>SqlStreamingAction[Vector[DataMart], DataMart, Effect]= { (sD,eD,acd) =>
    val startDateTime=sD.map("'" + _ + "'").mkString(",")
    val _acd=acd.map("'" + _ + "'").mkString(",")
    sql""" select count(distinct s.cfi_session_id) as count,
   if(sum(q.handled)>=1, 1, 0) as handled, IF(max(q.handled) = 0 , 1,
   0) as abandoned,
   sum(q.talk_duration_sec) as talk_duration,

   s.client_id,
   time(q.entry_datetime) as time,
   min(q.entry_datetime) as entry_time,

max(q.exit_datetime) as exit_time,
   timestampdiff(SECOND,
   min(q.entry_datetime),

max(q.connect_datetime)) as answer_delay,
   timestampdiff(SECOND,
   min(q.entry_datetime),

max(q.exit_datetime)) as abandon_time,
   sum(s.duration_sec) as total_duration,

date(q.entry_datetime) as date,
   IFNULL(e.service_level,
   0) as service_level_sec,

IF(sum(q.transfer)>=1,
   1,
   0) as transfer,
   e.wrapuptime,
   e.queue_id,

a.extname as extname ,
a.extnum as extnum,
   a.acdtype as acdtype,
   IF(s.handled,
   IFNULL(IF(e.service_level > 0,

  answer_delay_sec <= e.service_level,
   1),
   0),
   0) AS service_level
FROM
   queue_activity q
JOIN

session_activity s
      ON (
         q.cfi_session_id = s.cfi_session_id
      )
JOIN
   d_queue e

 ON (
         q.queue_id = e.queue_id
      )
JOIN
   context.acds a
      ON (
         q.queue_id = a.id
      )

WHERE
   s.client_id = ${Tenant.clientId}
   AND s.complete_datetime IS NOT NULL AND q.queue_id IN (#${_acd}) AND date(q.entry_datetime) in (#${startDateTime}) and time(q.entry_datetime) >= ("00:00:00")  GROUP BY s.cfi_session_id """.as[DataMart]
  }

  val dataMartDateFilter:(String,String,Seq[String]) => SqlStreamingAction[Vector[DataMart], DataMart, Effect]={ (startDate,endDate,acd)=>
    val _acd=acd.map("'" + _ + "'").mkString(",")
    sql"""  select count(distinct s.cfi_session_id) as count,
   if(sum(q.handled)>=1, 1, 0) as handled, IF(max(q.handled) = 0 , 1,
   0) as abandoned,
   sum(q.talk_duration_sec) as talk_duration,

   s.client_id,
   time(q.entry_datetime) as time,
   min(q.entry_datetime) as entry_time,

max(q.exit_datetime) as exit_time,
   timestampdiff(SECOND,
   min(q.entry_datetime),

max(q.connect_datetime)) as answer_delay,
   timestampdiff(SECOND,
   min(q.entry_datetime),

max(q.exit_datetime)) as abandon_time,
   sum(s.duration_sec) as total_duration,

date(q.entry_datetime) as date,
   IFNULL(e.service_level,
   0) as service_level_sec,

IF(sum(q.transfer)>=1,
   1,
   0) as transfer,
   e.wrapuptime,
   e.queue_id,

a.extname as extname ,
a.extnum as extnum,
   a.acdtype as acdtype,
   IF(s.handled,
   IFNULL(IF(e.service_level > 0,

  answer_delay_sec <= e.service_level,
   1),
   0),
   0) AS service_level
FROM
   queue_activity q
JOIN

session_activity s
      ON (
         q.cfi_session_id = s.cfi_session_id
      )
JOIN
   d_queue e

 ON (
         q.queue_id = e.queue_id
      )
JOIN
   context.acds a
      ON (
         q.queue_id = a.id
      )

WHERE
   s.client_id = ${Tenant.clientId}
   AND s.complete_datetime IS NOT NULL
          AND q.queue_id IN (#${_acd}) AND date(q.entry_datetime) >= ${startDate} and time(q.entry_datetime) >= ("00:00:00") and date(q.entry_datetime) <= ${endDate}
             GROUP BY
             s.cfi_session_id """.as[DataMart]
  }

  val dataMartTimeFilter:(String,Seq[String]) => SqlStreamingAction[Vector[DataMart], DataMart, Effect]={ (startDate,acd)=>
    val _acd=acd.map("'" + _ + "'").mkString(",")
    sql"""  select count(distinct s.cfi_session_id) as count,
   if(sum(q.handled)>=1, 1, 0) as handled, IF(max(q.handled) = 0 , 1,
   0) as abandoned,
   sum(q.talk_duration_sec) as talk_duration,

   s.client_id,
   time(q.entry_datetime) as time,
   min(q.entry_datetime) as entry_time,

max(q.exit_datetime) as exit_time,
   timestampdiff(SECOND,
   min(q.entry_datetime),

max(q.connect_datetime)) as answer_delay,
   timestampdiff(SECOND,
   min(q.entry_datetime),

max(q.exit_datetime)) as abandon_time,
   sum(s.duration_sec) as total_duration,

date(q.entry_datetime) as date,
   IFNULL(e.service_level,
   0) as service_level_sec,

IF(sum(q.transfer)>=1,
   1,
   0) as transfer,
   e.wrapuptime,
   e.queue_id,

a.extname as extname ,
a.extnum as extnum,
   a.acdtype as acdtype,
   IF(s.handled,
   IFNULL(IF(e.service_level > 0,

  answer_delay_sec <= e.service_level,
   1),
   0),
   0) AS service_level
FROM
   queue_activity q
JOIN

session_activity s
      ON (
         q.cfi_session_id = s.cfi_session_id
      )
JOIN
   d_queue e

 ON (
         q.queue_id = e.queue_id
      )
JOIN
   context.acds a
      ON (
         q.queue_id = a.id
      )

WHERE
   s.client_id = ${Tenant.clientId}
   AND s.complete_datetime IS NOT NULL
          AND q.queue_id IN (#${_acd}) AND date(q.entry_datetime) = ${startDate} and time(q.entry_datetime) >= ("00:00:00")
             GROUP BY
             s.cfi_session_id """.as[DataMart]
  }

val fetchAcds:(String,String)=>SqlStreamingAction[Vector[Acds], Acds, Effect]={(cID,calls)=>
  sql"""SELECT Acd.extnum,Acd.extname ,Acd.id FROM     acds AS Acd WHERE Acd.active != 2 AND Acd.queue_type IN ('call') AND Acd.client_id = ${cID}
    order by CAST(concat('', `extnum`) as UNSIGNED)""".as[Acds]
}
  val abandonedCallsFilter:(String,String,Seq[String],Seq[String])=>SqlStreamingAction[Vector[DataMartAbandoned],DataMartAbandoned,Effect] ={(sT,eT,acds,reasons)=>
    val _acd=acds.map("'" + _ + "'").mkString(",")
    val _reason=reasons.map("'" + _ + "'").mkString(",")
    sql"""select * from (SELECT DISTINCT sa.begin_datetime as begin,  qa.entry_datetime as entry, Acd.queue_name, sa.extern_source as ani, sa.extern_dest as dnis,  qa.queue_id as qId,sa.duration_sec as total_time,
      sa.complete_datetime as complete, case qa.result  when 'timeout' then 'Timeout' when 'hangup' then 'Hang Up'
      when 'abandoned' then 'Hang Up' when 'dtmf_exit' then 'DTMF Key' else '' end as abandon_reason,
     IFNULL(qa.exit_datetime, sa.complete_datetime) as abandon_time, qa.total_duration_sec AS abandon_duration, qa.redirect_name AS redirect
     FROM datamart.queue_activity qa INNER JOIN session_activity AS sa ON (qa.cfi_session_id = sa.cfi_session_id) INNER JOIN
     d_queue AS Acd ON (Acd.queue_id = qa.queue_id) WHERE sa.client_id = ${Tenant.clientId} AND sa.complete_datetime IS NOT NULL and date(qa.entry_datetime) >= ${sT} and date(qa.entry_datetime) <= ${eT} and qa.queue_id IN  (#${_acd})
     AND IFNULL(qa.handled, 0) = 0) abnd where abnd.abandon_reason IN (#${_reason})""".as[DataMartAbandoned]
  }
}
