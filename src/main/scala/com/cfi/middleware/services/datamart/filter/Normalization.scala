package com.cfi.middleware.services.datamart.filter

import java.util

import com.cfi.middleware.model.{DataMartAbandoned, DataMart}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by AravindKrishna on 15/12/15.
  */
trait Normalization extends Filter{

  def calculateForHour(datamarts:Vector[DataMart],value:Array[(String,String)],result:ArrayBuffer[java.util.Map[String,Object]]):ArrayBuffer[java.util.Map[String,Object]]={
    value.foreach { h =>
      val objectMap = new util.HashMap[String, Object]
      objectMap.put("Time", h._1 + "-" + h._2)
      val data = datamarts.filter(t => t.time >= h._1 + ":00" && t.time <= h._2 + ":00")
      result.+=(calculateWithNormlizedData(data,objectMap))
    }
    val _periodTotal=datamarts.foldLeft(0, 0, 0, 0, 0, 0, 0, 0, 0)((z, a) => (z._1 + a.count, z._2 + a.handled, z._3 + a.abandoned,
      if (a.abandoned == 1) z._4 + a.abandon_time else z._4, z._5 + a.answer_delay, if (a.handled == 1) z._6 + a.talk_duration else z._6, z._7 + a.service_level, if (a.handled == 1) z._8 + a.talk_duration else z._8, z._9 + a.transfer))
    result.+=(periodTotal(_periodTotal))
    result
  }
  def periodTotal(sumM:(Int,Int,Int,Int,Int,Int,Int,Int,Int))={
    val periodTotal=new util.HashMap[String,Object]()
    periodTotal.put("Time","Period Total")
    periodTotal.put("Calls", sumM._1 + "")
    periodTotal.put("Hndl", sumM._2 + "")
    periodTotal.put("Abnd", sumM._3 + "")
    periodTotal.put("Abnd %", convertFloatDecimalPoint(((sumM._3.toFloat / sumM._1.toFloat) * 100)) + "%")
    periodTotal.put("Avg Abnd", convertSecondsToFormated(sumM._4.toFloat, sumM._3.toFloat))
    periodTotal.put("ASA", convertSecondsToFormated(sumM._5.toFloat, sumM._2.toFloat))
    periodTotal.put("Total Talk", convertSecondsToFormated(sumM._6.toFloat, 1.toFloat))
    periodTotal.put("Avg Time", convertSecondsToFormated(sumM._6.toFloat, sumM._2.toFloat))
    periodTotal.put("Srv Lvl %", convertFloatDecimalPoint(((sumM._7.toFloat / sumM._1.toFloat) * 100)) + "%")
    periodTotal.put("Occ . %", convertFloatDecimalPoint(((sumM._8.toFloat / sumM._2.toFloat) / 100)) + "%")
    periodTotal
  }

  def  averageOfDayOfWeek(records:Vector[DataMart],days:Seq[String])={
    calculateForHour(records,hourFilter,new ArrayBuffer[util.Map[String, Object]]())
  }

  def calculateWithNormlizedData(data:Vector[DataMart],objectMap:java.util.Map[String,Object])= {
    data.length > 0 match {
      case true => val sumM = data.foldLeft(0, 0, 0, 0, 0, 0, 0, 0, 0)((z, a) => (z._1 + a.count, z._2 + a.handled, z._3 + a.abandoned,
        if (a.abandoned == 1) z._4 + a.abandon_time else z._4, z._5 + a.answer_delay, if (a.handled == 1) z._6 + a.talk_duration else z._6, z._7 + a.service_level, if (a.handled == 1) z._8 + a.talk_duration else z._8, z._9 + a.transfer))
        objectMap.put("Calls", sumM._1 + "")
        objectMap.put("Hndl", sumM._2 + "")
        objectMap.put("Abnd", sumM._3 + "")
        objectMap.put("Abnd %", convertFloatDecimalPoint(((sumM._3.toFloat / sumM._1.toFloat) * 100)) + "%")
        objectMap.put("Avg Abnd", convertSecondsToFormated(sumM._4.toFloat, sumM._3.toFloat))
        objectMap.put("ASA", convertSecondsToFormated(sumM._5.toFloat, sumM._2.toFloat))
        objectMap.put("Total Talk", convertSecondsToFormated(sumM._6.toFloat, 1.toFloat))
        objectMap.put("Avg Time", convertSecondsToFormated(sumM._6.toFloat, sumM._2.toFloat))
        objectMap.put("Srv Lvl %", convertFloatDecimalPoint(((sumM._7.toFloat / sumM._1.toFloat) * 100)) + "%")
        objectMap.put("Occ . %", convertFloatDecimalPoint(((sumM._8.toFloat / sumM._2.toFloat) / 100)) + "%")
        objectMap.put("Transfer Out", sumM._9 + "")
      case false => objectMap.put("Calls", 0 + "")
        objectMap.put("Hndl", 0 + "")
        objectMap.put("Abnd", 0 + "")
        objectMap.put("Abnd %", "0%")
        objectMap.put("Avg Abnd", "00:00:00")
        objectMap.put("ASA", "00:00:00")
        objectMap.put("Total Talk", "00:00:00")
        objectMap.put("Avg Time", "00:00:00")
        objectMap.put("Srv Lvl %", "0%")
        objectMap.put("Occ . %", "0%")
        objectMap.put("Transfer Out", 0 + "")
    }
    objectMap
  }

  def calculateQueueDisposition(datamarts:Vector[DataMart],objectMap:java.util.Map[String,Object]): java.util.Map[String,Object] ={
    datamarts.length > 0 match {
      case true=>val sumM= datamarts.foldLeft(0,0,0,0)((z,a)=> (z._1+a.count,z._2+a.handled,z._3+a.abandoned,if(a.abandoned ==1) z._4+a.answer_delay else z._4))
        objectMap.put("Calls", sumM._1 + "")
        objectMap.put("Hndl", sumM._2 + "")
        objectMap.put("Abnd", sumM._3 + "")
        objectMap.put("answer", sumM._4 + "")
        objectMap.put("queueNum", datamarts.take(1).head.extnum + "")
        objectMap.put("queueName", datamarts.take(1).head.extname+ "")
      case false=>
        objectMap.put("Calls", 0 + "")
        objectMap.put("Hndl", 0 + "")
        objectMap.put("Abnd", 0 + "")
        objectMap.put("answer", 0 + "")
        objectMap.put("queueNum", 0 + "")
        objectMap.put("queueName",  "")
    }
    objectMap
  }

  def normalizeQueueDisposition(datamarts:Vector[DataMart],acds:Seq[String],result:ArrayBuffer[java.util.Map[String,Object]]):ArrayBuffer[java.util.Map[String,Object]]={
    acds.foreach { z =>
      val objectMap = new util.HashMap[String, Object]
      val data=datamarts.filter(_.queue_id==z.toLong)
      result.+=(calculateQueueDisposition(data,objectMap))
    }
  result
  }

  def normalizeAbandonedCalls(abandoneds:Vector[DataMartAbandoned],result:ArrayBuffer[java.util.Map[String,Object]]):ArrayBuffer[java.util.Map[String,Object]]={
    abandoneds.foreach{data=>
      val objectMap = new util.HashMap[String, Object]
      objectMap.put("Call Start",data.begin);
      objectMap.put("ACD Entry",data.entry);
      objectMap.put("CNAM",data.callSource.split("-").head);
      objectMap.put("Caller Id",data.callSource.split("-").last);
      objectMap.put("ACD Start",data.queueName);
      objectMap.put("Abnd Time",data.abandonTime);
      objectMap.put("Abnd Reason",data.abandonReason);
      objectMap.put("Redirect",data.redirect);
      objectMap.put("Call End",data.complete);
      objectMap.put("Duration To Abnd",convertSecondsToFormated(data.abandonDuration.toFloat,1.toFloat));
      objectMap.put("Total Call Duration",convertSecondsToFormated(data.totalTime.toFloat,1.toFloat));
      result.+=(objectMap)
    }
    result
  }

  def calculateNormalizedSecondLevelAbandonedCalls(abandoneds:Vector[DataMartAbandoned],objectMap:java.util.Map[String,Object],queue:(Int,String)): java.util.Map[String,Object]={
      abandoneds.length > 0 match {
        case true=>
          val sumM=abandoneds.foldLeft(0,0,0,0,0,0,0)((z,a)=> (if(a.abandonReason.equals("Hang Up")) z._1+1 else z._1,if(a.abandonReason.equals("DTMF Key")) z._2+1 else z._2,if(a.abandonReason.equals("Timeout")) z._3+1 else z._3,if(a.abandonReason.equals("Hang Up")) z._4+a.abandonDuration else z._4,if(a.abandonReason.equals("DTMF Key")) z._5+a.abandonDuration else z._5,if(a.abandonReason.equals("Timeout")) z._6+a.abandonDuration else z._6,z._7+a.abandonDuration))
          objectMap.put("HangUp",sumM._1+"")
          objectMap.put("DTMFKey",sumM._2+"")
          objectMap.put("Timeout",sumM._3+"")
          objectMap.put("HangUpDuration",sumM._4+"")
          objectMap.put("DTMFKeyDuration",sumM._5+"")
          objectMap.put("TimeoutDuration",sumM._6+"")
          objectMap.put("TotalDuration",sumM._7+"")
          objectMap.put("queueNum", queue._1+"")
          objectMap.put("queueName", queue._2)
        case false=>
          objectMap.put("HangUp",0+"")
          objectMap.put("DTMFKey",0+"")
          objectMap.put("Timeout",0+"")
          objectMap.put("HangUpDuration",0+"")
          objectMap.put("DTMFKeyDuration",0+"")
          objectMap.put("TimeoutDuration",0+"")
          objectMap.put("TotalDuration",0+"")
          objectMap.put("queueNum", queue._1+"")
          objectMap.put("queueName", queue._2)
      }
    objectMap
  }

  def normalizeSecondLevelAbandonedCalls(abandoneds:Vector[DataMartAbandoned],acds:Seq[String],reason:Seq[String],result:ArrayBuffer[java.util.Map[String,Object]]):ArrayBuffer[java.util.Map[String,Object]]={
    acds.foreach{z=>
      val objectMap = new util.HashMap[String, Object]
      val data=abandoneds.filter(_.queueId == z.toInt)
      data.length > 0 match {
        case true=>
          result.+=:(calculateNormalizedSecondLevelAbandonedCalls(data,objectMap,(data.head.queueNum,data.head.queueName)))
        case false=>
      }
    }
    result
  }

  def calculateForDays(datamarts:Vector[DataMart],startDate:String,endDate:String,result:ArrayBuffer[java.util.Map[String,Object]])={
    val YYMM=startDate.split("-").take(2).foldLeft("")((z,a)=> z+a+"-")
    val _mapDataBuffer=new ArrayBuffer[DataMart]()
    for(i<- startDate.split("-").last.toInt to endDate.split("-").last.toInt){
      val objectMap = new util.HashMap[String, Object]
      objectMap.put("Time", YYMM+i)
      val data = datamarts.filter(t => t.date.equals(YYMM+i))
      data.foreach(z=> _mapDataBuffer.+=(z))
      result.+=(calculateWithNormlizedData(data,objectMap))
    }
    val _periodTotal=_mapDataBuffer.foldLeft(0, 0, 0, 0, 0, 0, 0, 0, 0)((z, a) => (z._1 + a.count, z._2 + a.handled, z._3 + a.abandoned,
      if (a.abandoned == 1) z._4 + a.abandon_time else z._4, z._5 + a.answer_delay, if (a.handled == 1) z._6 + a.talk_duration else z._6, z._7 + a.service_level, if (a.handled == 1) z._8 + a.talk_duration else z._8, z._9 + a.transfer))
    result.+=(periodTotal(_periodTotal))
    result
  }
}
