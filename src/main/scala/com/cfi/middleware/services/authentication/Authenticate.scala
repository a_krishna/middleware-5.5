package com.cfi.middleware.services.authentication

import javax.servlet.http.{Cookie, HttpServletResponse, HttpServletRequest, HttpServlet}

import scala.collection.mutable

/**
  * Created by AravindKrishna on 22/12/15.
  */
case class UserSession(name:String,tenant:String)

class Login extends HttpServlet{
  override def doGet(request:HttpServletRequest,response:HttpServletResponse){
    response.setContentType("text/html")
    val  out=response.getWriter()
    request.getRequestDispatcher("login.html").include(request, response)
    out.close()
  }
}
class Authenticate extends HttpServlet{
  override def doPost(request:HttpServletRequest,response:HttpServletResponse){
    response.setContentType("text/html")

    val  out=response.getWriter()
    val name=request.getParameter("name")

    request.getParameter("password") match {
      case "admin123"=>
        val session=request.getSession()
        val uuid=SessionHandler.uuid
        SessionHandler._sessionMap.put(uuid,UserSession(name,request.getParameter("tenant")))
        session.setAttribute("name",name)
        session.setAttribute("session-id",uuid)
        response.addCookie(new Cookie("_id",uuid))
        request.getRequestDispatcher("index.html").include(request, response)
      case _=>
        out.print("Sorry, username or password error!")
        request.getRequestDispatcher("login.html").include(request, response)
    }
    out.close()
  }

}

class Profile extends HttpServlet{
  override def doGet(request:HttpServletRequest,response:HttpServletResponse){
    response.setContentType("text/html")
    val  out=response.getWriter()
    val session=request.getSession(false)
    SessionHandler._sessionMap.contains(session.getAttribute("session-id").toString) match {
      case true=>
        request.getRequestDispatcher("index.html").include(request, response)
      case false=>
        out.print("Invalid Session")
        request.getRequestDispatcher("login.html").include(request, response)
    }
    out.close()
  }
}

class LogOut extends HttpServlet{
  override def doGet(request:HttpServletRequest,response:HttpServletResponse){
    response.setContentType("text/html")
    val  out=response.getWriter()

    request.getRequestDispatcher("login.html").include(request, response)

    val session=request.getSession()
    SessionHandler._sessionMap.remove(session.getAttribute("session-id").toString)
    session.invalidate()

    out.print("You are successfully logged out!")

    out.close()
  }
}

object SessionHandler{
  val _sessionMap=new mutable.HashMap[String,UserSession]()
  def uuid = java.util.UUID.randomUUID.toString
}
