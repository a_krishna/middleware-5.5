package com.cfi.middleware.services

import javax.annotation.PreDestroy
import javax.inject.Singleton

import akka.actor.{ActorRef, Props, ActorSystem, Actor}
import com.cfi.middleware.config.akka.{AkkaModule, ConfigModule}
import com.cfi.middleware.config.atmosphere.CustomConfig
import com.cfi.middleware.message._
import com.cfi.middleware.model.{QueueCalls, JacksonDecoder, JacksonEncoder, Transaction}
import com.cfi.middleware.services.authentication.SessionHandler
import com.cfi.middleware.services.consumer.{CFIConstant, CFIConsumer}
import com.cfi.middleware.services.datamart.{DataMartAccessActor, Tenant}
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.inject.{Injector, Guice}
import org.atmosphere.config.service.{Disconnect, Ready, PathParam, ManagedService}
import org.atmosphere.cpr.{Broadcaster, AtmosphereResourceEvent, AtmosphereResource, BroadcasterFactory}

/**
  * Created by AravindKrishna on 5/12/15.
  */
@CustomConfig
@ManagedService(path = "/domain/{clientId: [a-zA-Z][a-zA-Z_0-9_-]*}")
class ManagedActor {
  val LOGGER = org.slf4j.LoggerFactory.getLogger(classOf[ManagedActor])
  val SERVICE = "/domain/"
  val injector:Injector = Guice.createInjector(new ConfigModule, new AkkaModule)
  lazy val system:ActorSystem = injector.getInstance(classOf[ActorSystem])
  var flag=false
  lazy val managedActor = system.actorOf(Props(new Actor {
    override def receive: Receive = {
      case AgentEventSnapShot(agentEvents) =>
        val message = agentEvents.foldLeft(Array.empty[java.util.Map[String, Object]])((z, a) => z ++ Array(a.getSnapsotData))
        onBroadcastSnapShotResponse(CFIConstant.EXCHANGE_NAME_AGENT_EVENT+":create", message)
      case QueueEventSnapShot(queueEvents) =>
        val message = queueEvents.foldLeft(Array.empty[java.util.Map[String, Object]])((z, a) => z ++ Array(a.getSnapShotdata))
        onBroadcastSnapShotResponse(CFIConstant.EXCHANGE_NAME_QUEUE_EVENT+":create", message)
      case AgentStatsSnapShot(agentStats) =>
      case QueueStatsSnapShot(queueStats) =>
        val message= queueStats.foldLeft(Array.empty[java.util.Map[String,Object]])((z,a)=> z ++ Array(a.getMapData))
        onBroadcastSnapShotResponse(CFIConstant.EXCHANGE_NAME_QUEUE_STATS+":create",message)
      case AgentEventFeed(agentEvent, action) =>
        action match {
          case "offer_call" | "accept_call" | "hangup" | "wrapup_complete" => onBroadCastFeed(CFIConstant.EXCHANGE_NAME_AGENT_EVENT + ":" + action, agentEvent.getCallDetails)
          case _ => onBroadCastFeed(CFIConstant.EXCHANGE_NAME_AGENT_EVENT + ":" + action, agentEvent.getSnapsotData)
        }
      case UnAgentEventFeed(obj,action)=>onBroadCastFeed(CFIConstant.EXCHANGE_NAME_AGENT_EVENT+":"+action,obj)

      case QueueEventFeed(queueEvent, action) => onBroadCastFeed(CFIConstant.EXCHANGE_NAME_QUEUE_EVENT + ":" + action, queueEvent.getSnapShotdata)
      case AgentStatsFeed(agentStats, action) =>
      case QueueStatsFeed(queueStats, action) => onBroadCastFeed(CFIConstant.EXCHANGE_NAME_QUEUE_STATS+":"+action,queueStats.getMapData)
      case QueueGraphSnapShot(queueGraph)=>
          val message=queueGraph.foldLeft(Array.empty[java.util.Map[String,Object]])((z,a)=> z ++ Array(a.getSnapShot))
        onBroadcastSnapShotResponse(CFIConstant.EXCHANGE_NAME_QUEUE_EVENT+":graph",message)

      case QueuedCallsResponse(tID,_records)=>onBroadCastStatic(tID,"static_snapshot","queued_calls",_records)
      case AcdsResponse(tID,_records)=>onBroadCastStatic(tID,"static_snapshot","acds",_records)
      case QueuedCallsDispositionResponse(tID,_records)=>onBroadCastStatic(tID,"static_disposition","queued_calls",_records)
      case AbandonedCallsDispositionResponse(tID,_records)=>onBroadCastStatic(tID,"static_disposition","abandoned_calls",_records)
      case AbandonedCallsSecondDispositionResponse(tID,_records)=>onBroadCastStatic(tID,"static_disposition","abnd_reason_calls",_records)
      case _ =>
    }
  }))
    var dataMartAccessActor:ActorRef=null
     var  consumerActor:ActorRef =null

  @PathParam("clientId") var clientId: String = _
  private var factory: BroadcasterFactory = null

  @Ready
  def onReady(r: AtmosphereResource) {
    factory = r.getAtmosphereConfig.getBroadcasterFactory
    LOGGER.debug(s"Browser Connected: ${r.uuid()} ")
    LOGGER.debug(s"Browser Factory Class Name: ${factory.getClass.getName} ")
    LOGGER.debug(s"Browser ID: ${factory.get().getID} ")
    LOGGER.debug(s"Client ID: ${r.getRequest.getRequestURI()} ")
    LOGGER.debug(s"Client ID from PATH PARAM : ${clientId} ")
    SessionHandler._sessionMap.contains(clientId) match {
      case true=>
          dataMartAccessActor=system.actorOf(Props(new DataMartAccessActor(Tenant.clientId,managedActor)))
          consumerActor = system.actorOf(Props(new CFIConsumer(clientId,managedActor)))
      case false=>r.close()
        r.getAtmosphereHandler.destroy()
        r.getResponse.sendRedirect("/Login")
    }
  }

  @Disconnect
  def onDisconnect(event: AtmosphereResourceEvent): Unit = {
    event.isCancelled match {
      case true => LOGGER.info(s"BROWSER UNEXPECTEDLY DISCONNECTED: ${event.getResource.uuid()} ")
//        SessionHandler._sessionMap.remove(clientId)
        system.shutdown()
      case false => event.isClosedByClient match {
        case true => LOGGER.info(s"BROWSER CLOSED THE  CONNECTION: ${event.getResource.uuid()} ")
//          SessionHandler._sessionMap.remove(clientId)
          system.shutdown()
        case false => LOGGER.info(s"BROWSER  DISCONNECTED: ${event.getResource.uuid()} ")
//          SessionHandler._sessionMap.remove(clientId)
          system.shutdown()
      }
    }
  }

  @org.atmosphere.config.service.Message(encoders = Array(classOf[JacksonEncoder]), decoders = Array(classOf[JacksonDecoder]))
  def onMessage(transaction: Transaction) = {
    transaction.getProcess.contains("request") match {
      case true =>
        LOGGER.debug(s"DATA RECEIVED ${transaction.toString}")
        onProcessRequest(transaction)
      case false => LOGGER.debug(s"DATA SENT ${transaction.toString}")
        transaction
    }
  }

  @PreDestroy
  def onPreDestroy(): Unit ={
    system.shutdown()
    LOGGER.debug(s"DESTROYED SERVICE ")
  }

  def onProcessRequest(transaction: Transaction) = transaction.getCommand match {
    case "live_init" => transaction.getCommandType match {
      case "agent_event" => consumerActor ! SnapShotAgentEvent
      case "queue_event" => consumerActor ! SnapShotQueueEvent
      case "agent_stats" => consumerActor ! SnapShotAgentStats
      case "queue_stats" => consumerActor ! SnapShotQueueStats
      case "queue_graph" => consumerActor ! SnapShotQueueGraph
      case "initialize"=>consumerActor ! InitalizeConusmer
      case _ =>
    }
    case "live_feed" => transaction.getCommandType match {
      case "agent_event" => consumerActor ! FeedAgentEvent
      case "queue_event" => consumerActor ! FeedQueueEvent
      case "agent_stats" => consumerActor ! FeedAgentStats
      case "queue_stats" => consumerActor ! FeedQueueStats
      case _ =>
    }
    case "live_report" =>
    case "static_init" =>transaction.getCommandType match {
      case "queued_calls" =>
        val _mapper=new ObjectMapper()

        val reqMsg=_mapper.readValue(transaction.getMessage.toString,classOf[QueueCalls])
        dataMartAccessActor ! QueryQueuedCalls(transaction.getTransactionId,reqMsg.getStartTime,reqMsg.getEndTime,reqMsg.getAcd,reqMsg.getPeriod,reqMsg.getTimeDelay)
      case "acd" => dataMartAccessActor ! FetchAcds(transaction.getTransactionId)
      case "initialize"=>  dataMartAccessActor ! InitDataMart
      case _ =>
    }
    case "static_disposition"=> transaction.getCommandType match {
      case "queue_disposition"=>
        val _mapper=new ObjectMapper()
        val reqMsg=_mapper.readValue(transaction.getMessage.toString,classOf[QueueCalls])
        dataMartAccessActor ! QueryQueuedDisposition(transaction.getTransactionId,reqMsg.getStartTime,reqMsg.getEndTime,reqMsg.getAcd,reqMsg.getReason)
      case _=>
    }
    case _ =>
  }

  def onBroadcastSnapShotResponse(commandType: String, message: Array[java.util.Map[String, Object]]): Unit = {
    val produce = new Transaction
    val b: Broadcaster = factory.lookup(s"$SERVICE${clientId}", true)
    produce.setProcess("response")
    produce.setCommand("live_snapshot")
    produce.setCommandType(commandType)
    produce.setMessage(message)
    b.broadcast(produce)
  }

  def onBroadCastFeed(commandType: String, message: java.util.Map[String, Object]): Unit = {
    val produce = new Transaction
    val b: Broadcaster = factory.lookup(s"$SERVICE${clientId}", true)
    produce.setProcess("response")
    produce.setCommand("live_fed")
    produce.setCommandType(commandType)
    produce.setMessage(message)
    b.broadcast(produce)
  }

  def onBroadCastStatic(tID:String,command:String,commandType: String, message:Array[ java.util.Map[String, Object]]): Unit = {
    val produce = new Transaction
    val b: Broadcaster = factory.lookup(s"$SERVICE${clientId}", true)
    produce.setTransactionId(tID)
    produce.setProcess("response")
    produce.setCommand(command)
    produce.setCommandType(commandType)
    produce.setMessage(message)
    b.broadcast(produce)
  }

  Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
    override def run(): Unit =
      onPreDestroy()
  }))
}
