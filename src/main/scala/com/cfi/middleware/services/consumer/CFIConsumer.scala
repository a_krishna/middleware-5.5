package com.cfi.middleware.services.consumer

import java.util

import akka.actor.{Props, Actor, ActorLogging, ActorRef}
import akka.event.Logging
import com.cfi.middleware.message._
import com.cfi.middleware.model._
import com.cfi.middleware.services.authentication.SessionHandler
import com.cfi.middleware.util.MainReflection
import com.rabbitmq.client._

import scala.collection.{mutable, Set}

/**
  * Created by AravindKrishna on 5/12/15.
  */
class CFIConsumer(clientId: String,ref: ActorRef) extends Actor with ActorLogging {

  import CFIConstant._

  val LOGGER = Logging.getLogger(context.system, this)
  val hostServer = context.system.settings.config.getString("cfi.consumer.connection-server")
  val portNum = context.system.settings.config.getInt("cfi.consumer.connection-port")
  var virtual_host: String = "/" + "362781"
  var QUEUE_NAME: String = ""
  var factory: ConnectionFactory = null
  var agentEventState = LiveState.DISABLE
  var queueEventState = LiveState.DISABLE
  val graphFSMAgents=new mutable.HashMap[String,ActorRef]()
  var consumerSnapShot:CFISnapShot=null
  var connection:Connection=null
  var channel:Channel=null

  def initiate(): Unit = {
    consumerSnapShot=new CFISnapShot
    SessionHandler._sessionMap.get(clientId) match {
      case Some(user)=>
        virtual_host="/"+user.tenant
      case None=>
    }
    factory = new ConnectionFactory
    factory.setHost(hostServer)
    factory.setPort(portNum)
    factory.setVirtualHost(virtual_host)
     connection = factory.newConnection()
     channel = connection.createChannel
    QUEUE_NAME = channel.queueDeclare.getQueue
    channel.queueBind(QUEUE_NAME, EXCHANGE_NAME_QUEUE_STATS, "#")
    channel.queueBind(QUEUE_NAME, EXCHANGE_NAME_QUEUE_EVENT, "#")
    channel.queueBind(QUEUE_NAME, EXCHANGE_NAME_AGENT_EVENT, "#")
    channel.queueBind(QUEUE_NAME, EXCHANGE_NAME_AGENT_STATS, "#")
    LOGGER.debug("EXCHANGES SUBSCRIBED")
    startBasicConsume(channel)
  }

  def startBasicConsume(channel: Channel): Unit ={
    val numPattern = "[0-9]+".r
    val consume = new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
        super.handleDelivery(consumerTag, envelope, properties, body)
        envelope.getRoutingKey.matches(numPattern.regex) match {
          case true=>
            val headers=properties.getHeaders
            val action=headers.get("action").toString
            LOGGER.debug(s" RECVIEVED MSG headers ${headers}  routing_key ${envelope.getRoutingKey} exchange ${envelope.getExchange}")
            envelope.getExchange match {
              case EXCHANGE_NAME_AGENT_EVENT=>
                self ! RecvChunkAgentEvent(headers,envelope.getRoutingKey,action)
              case EXCHANGE_NAME_QUEUE_EVENT=>
                self ! RecvChunkQueueEvent(headers,envelope.getRoutingKey,action)
              case EXCHANGE_NAME_AGENT_STATS=>
                self ! RecvChunkAgentStats(headers,envelope.getRoutingKey,action)
              case EXCHANGE_NAME_QUEUE_STATS=>
                self ! RecvChunkQueueStats(headers,envelope.getRoutingKey,action)
              case _=>
            }
          case false=>
        }
      }
    }
    channel.basicConsume(QUEUE_NAME, true, consume)
  }

  override def preStart()={
    LOGGER.debug(s"CONSUMER INITIATED ")
    initiate()
  }
  override def postStop() = {
    LOGGER.debug(s"CONSUMER CONNECTION CLOSED ")
    channel.close()
    connection.close()
  }
  def handleGraphEvent()={
    queueEventState match {
      case LiveState.DISABLE=>
      case LiveState.FEED=>
        ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
      case LiveState.STOP=>
    }
  }
  def handleAgentEvent(agentEvent: AgentEvent, action: String) = {
    agentEventState match {
      case LiveState.DISABLE =>
      case LiveState.FEED =>
        ref ! AgentEventFeed(agentEvent, action)
      case LiveState.STOP =>
    }
  }
  def unhandleAgentEvent(objectMap:util.Map[String,Object], action: String) = {
    agentEventState match {
      case LiveState.DISABLE =>
      case LiveState.FEED =>
        ref ! UnAgentEventFeed(objectMap, action)
      case LiveState.STOP =>
    }
  }

  def handleAgentStats(agentStats: AgentStats, action: String,agentEvent: AgentEvent) = {
    agentEventState match {
      case LiveState.DISABLE =>
      case LiveState.FEED =>
        agentEvent.setHandled_in_last_day(agentStats.getHandled_in_last_day)
            ref ! AgentEventFeed(agentEvent, "agent_stats_" + action)
      case LiveState.STOP =>
    }
  }

  def handleQueueEvent(queueEvent: QueueEvent, action: String) = {
    queueEventState match {
      case LiveState.DISABLE =>
      case LiveState.FEED =>
        ref ! QueueEventFeed(queueEvent, action)
      case LiveState.STOP =>
    }
  }

  def handleQueueStats(queueStats: QueueStats, action: String,queueEvent: QueueEvent) = {
    queueEventState match {
      case LiveState.DISABLE =>
      case LiveState.FEED =>
        queueEvent.setAbandoned_in_last_day(queueStats.getAbandoned_in_last_day)
        queueEvent.setOffered_in_last_day(queueStats.getOffered_in_last_day)
        queueEvent.setAgent_count(queueStats.getAgent_count)
        queueEvent.setAvailable_count(queueStats.getAvailable_count)
        queueEvent.setHandled_in_last_day(queueStats.getHandled_in_last_day)
            ref ! QueueEventFeed(queueEvent,"queue_stats_"+action)
        log.info(s"Queue Stats ${queueEvent.toString}")
      case LiveState.STOP =>

    }
  }

  import  graph._
  override def receive: Receive = {
    case RecvChunkAgentEvent(headers,routing_key,action)=> action  match {
      case ("create" | "login") =>
        val state = headers.get("state").asInstanceOf[util.HashMap[String, Object]]
        val agentEvent = MainReflection.classMapper("com.cfi.middleware.model.AgentEvent", state).asInstanceOf[AgentEvent]
        agentEvent.setFullname(state.get("fullname").toString)
        consumerSnapShot.agentEvents.add(agentEvent) match {
          case true =>
            val agentGraph=new AgentGraph(agentEvent.getExtension,NotReady)
            agentGraph.setQueues(agentEvent.getQueues.split(","))
            consumerSnapShot._agentGraph.add(agentGraph)
            graphFSMAgents.put(routing_key,context.system.actorOf(Props(new Graph(ref))))
            action match {
            case "login" => handleAgentEvent(agentEvent, action)
              self ! CreateQueueGraph(HandleAgentEvent(agentEvent.getExtension,agentEvent.getReady,agentEvent.getExternal_busy,agentEvent.getOn_a_call,agentEvent.getOn_wrap_up))
            case _ => LOGGER.debug(s"AGENT EVENT CREATE ${agentEvent}")
          }
          case false => LOGGER.debug(s"AGENT EVENT DUPLICATE ${agentEvent}")
        }

      case _ =>
        consumerSnapShot.agentEvents.zip(consumerSnapShot._agentGraph).find(z=>z._1.getExtension.equals(routing_key) && z._2.getExtension.equals(routing_key)) match {
          case Some(a)=>consumerSnapShot.updateAgentEvent(headers,routing_key,a._1) match {
            case Some(res)=>
              action match {
                case "logout"=>consumerSnapShot.agentEvents.remove(res)
                  self ! LogOutQueueGraph(a._2)
                  consumerSnapShot._agentGraph.remove(a._2)
                case _=> self ! CreateQueueGraph(HandleAgentEvent(a._2.getExtension,res.getReady.toInt,res.getExternal_busy.toInt,res.getOn_a_call.toInt,res.getOn_wrap_up.toInt))
              }
              handleAgentEvent(res,action)

            case None=>
          }
          case None=>
        }
    }
    case RecvChunkAgentStats(headers,routing_key,action)=>action match {
      case "create" =>
        val state = headers.get("state").asInstanceOf[util.HashMap[String, Object]]
        val agentStats = MainReflection.classMapper("com.cfi.middleware.model.AgentStats", state).asInstanceOf[AgentStats]
        agentStats.setExtension(routing_key)
        agentStats.setMsgType(action)
        consumerSnapShot._agentStats.add(agentStats) match {
          case true => LOGGER.debug(s"AGENT STATS {$agentStats }")
            consumerSnapShot.agentEvents.find(_.getExtension.equals(agentStats.getExtension)) match {
              case Some(aE)=>aE.setHandled_in_last_day(agentStats.getHandled_in_last_day)
              case None=>
            }
          case false =>
        }
      case _ =>
        consumerSnapShot.agentEvents.zip(consumerSnapShot._agentStats).find(z=> z._1.getExtension.equals(routing_key) && z._2.getExtension.equals(routing_key)) match {
          case Some(obj)=>
            consumerSnapShot.updateAgentStats(headers,routing_key,obj._2) match {
            case Some(res)=>
              handleAgentStats(res,action,obj._1)
            case None=>
          }
          case None=>
        }
    }
    case RecvChunkQueueEvent(headers,routing_key,action)=>action match {
      case "create" =>
        val state = headers.get("state").asInstanceOf[util.HashMap[String, Object]]
        val queueEvent = MainReflection.classMapper("com.cfi.middleware.model.QueueEvent", state).asInstanceOf[QueueEvent]
        queueEvent.setMsgType(action)
        consumerSnapShot.queueEvents.add(queueEvent) match {
          case true => LOGGER.debug(s"QUEUE EVENT ${queueEvent}")
            consumerSnapShot._queueStats.find(_.getExtension.equals(queueEvent.getExtension)) match {
              case Some(qS)=> queueEvent.setOffered_in_last_day(qS.getOffered_in_last_day)
                queueEvent.setAgent_count(qS.getAgent_count)
                queueEvent.setAvailable_count(qS.getAvailable_count)
                queueEvent.setAbandoned_in_last_day(qS.getAbandoned_in_last_day)
                queueEvent.setHandled_in_last_day(qS.getHandled_in_last_day)
                consumerSnapShot._queueGraph.add(new QueueGraph(queueEvent.getExtension,0))
//                handleGraphEvent()
                handleQueueEvent(queueEvent,action)
              case None=>
            }
          case false =>
        }
      case "delete"=>
        consumerSnapShot.queueEvents.zip(consumerSnapShot._queueGraph).find(z=>z._1.getExtension.equals(routing_key) && z._2.getExtension.equals(routing_key)) match {
        case Some(q)=> consumerSnapShot.queueEvents.remove(q._1)
          consumerSnapShot._queueGraph.remove(q._2)
          ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          handleQueueEvent(q._1,action)
        case None=>
      }
      case _ =>consumerSnapShot.queueEvents.find(_.getExtension.equals(routing_key)) match {
        case Some(qE)=>consumerSnapShot.updateQueueEvent(headers,routing_key,qE) match {
          case Some(res)=>handleQueueEvent(res,action)
          case None=>
        }
        case None=>
      }
    }
    case RecvChunkQueueStats(headers,routing_key,action)=>action match {
      case "create" =>
        val state = headers.get("state").asInstanceOf[util.HashMap[String, Object]]
        val queueStats = MainReflection.classMapper("com.cfi.middleware.model.QueueStats", state).asInstanceOf[QueueStats]
        queueStats.setExtension(routing_key)
        queueStats.setMsgType(action)
        consumerSnapShot._queueStats.add(queueStats) match {
          case true => LOGGER.debug(s"QUEUE STATS ${queueStats }")
          case false =>
        }
      case _ =>
        consumerSnapShot._queueStats.find(_.getExtension.equals(routing_key)) match {
          case Some(obj)=>
            consumerSnapShot.updateQueueStats(headers,routing_key,obj) match {
            case Some(res)=>
              consumerSnapShot.queueEvents.find(_.getExtension.equals(res.getExtension)) match {
                case Some(_res)=>
                  handleQueueStats(res,action,_res)
                case None=>
              }
            case None=>
          }
          case None=>
        }
    }
    case InitalizeConusmer =>
      LOGGER.info(s"CONUSMER INTIATED")
    case LoginQueueGraph(agentGraph)=>
      agentGraph.getQueues.foreach{agG=> consumerSnapShot._queueGraph.find(_.getExtension.equals(agG)) match {
        case Some(qG)=>qG.setReadyCount(qG.getReadyCount+1)
        case None=>
      }}
      ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
    case LogOutQueueGraph(agentGraph)=>
      agentGraph.getQueues.foreach{agG=> consumerSnapShot._queueGraph.find(_.getExtension.equals(agG)) match {
        case Some(qG)=>qG.setReadyCount(neglectNeg(qG.getReadyCount))
        case None=>
      }}
      ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
    case CreateQueueGraph(handleAgentEvent)=>
      graphFSMAgents.get(handleAgentEvent.extension) match {
        case Some(graphFSM)=>handleAgentEvent.ready match {
          case 0=> graphFSM ! MoveNotReady(handleAgentEvent.extension,consumerSnapShot)
          case 1=> (handleAgentEvent.externalBusy,handleAgentEvent.onCall,handleAgentEvent.wrapUp) match {
            case (0,0,0)=>graphFSM ! MoveReady(handleAgentEvent.extension,consumerSnapShot)
            case (0,0,1)=>graphFSM ! MoveWrapUp(handleAgentEvent.extension,consumerSnapShot)
            case (1,0,0)=>graphFSM ! MoveBusy(handleAgentEvent.extension,consumerSnapShot)
            case (0,1,0)=>graphFSM ! MoveBusy(handleAgentEvent.extension,consumerSnapShot)
            case (0,1,1)=>graphFSM ! MoveWrapUp(handleAgentEvent.extension,consumerSnapShot)
            case (1,1,0)=>graphFSM ! MoveBusy(handleAgentEvent.extension,consumerSnapShot)
            case (1,0,1)=>graphFSM ! MoveWrapUp(handleAgentEvent.extension,consumerSnapShot)
            case (1,1,1)=>graphFSM ! MoveWrapUp(handleAgentEvent.extension,consumerSnapShot)
            case _=>
          }
      }
        case None=>LOGGER.info("CANNOT NOT FOUND FSM AGENT")

    }
    case AddActorRef(_ref) =>
    case SnapShotAgentEvent => ref ! AgentEventSnapShot(consumerSnapShot.agentEvents)
    case SnapShotQueueEvent => ref ! QueueEventSnapShot(consumerSnapShot.queueEvents)
    case SnapShotQueueStats =>ref ! QueueStatsSnapShot(consumerSnapShot._queueStats)
    case SnapShotAgentStats =>
    case SnapShotQueueGraph =>
      LOGGER.info(s"  AGENT GRAPH ${consumerSnapShot._agentGraph.size}   QUEUE GRAPH ${consumerSnapShot._queueGraph.size}")
      consumerSnapShot.agentEvents.foreach{
        agent=> self ! CreateQueueGraph(HandleAgentEvent(agent.getExtension,agent.getReady.toInt,agent.getExternal_busy.toInt,agent.getOn_a_call.toInt,agent.getOn_wrap_up.toInt))
      }
      ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
    case FeedAgentEvent => agentEventState = LiveState.FEED
    case FeedQueueEvent => queueEventState = LiveState.FEED
    case FeedAgentStats =>
    case FeedQueueStats =>
    case StopAgentEvent => agentEventState = LiveState.STOP
    case StopAgentStats =>
    case StopQueueEvent => queueEventState = LiveState.STOP
    case StopQueueStats =>
    case ConsumerShutdown =>
  }


  def neglectNeg(a:Int):Int= a-1 < 0 match {
    case true=>0
    case false=>a-1
  }
}