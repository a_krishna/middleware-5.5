package com.cfi.middleware.services.consumer.graph

import akka.actor.{FSM, ActorLogging, Actor, ActorRef}
import com.cfi.middleware.message.QueueGraphSnapShot
import com.cfi.middleware.model.AgentGraph
import com.cfi.middleware.services.consumer.CFISnapShot

/**
  * Created by AravindKrishna on 10/12/15.
  */
sealed trait AgentState
case object NotReady extends AgentState
case object Busy extends AgentState
case object Ready extends AgentState
case object WrapUp extends AgentState
case class StartUp(msg:String)

case class  MoveReady(extension:String,consumerSnappShot:CFISnapShot)
case class  MoveNotReady(extension:String,consumerSnappShot:CFISnapShot)
case class  MoveBusy(extension:String,consumerSnappShot:CFISnapShot)
case class  MoveWrapUp(extension:String,consumerSnappShot:CFISnapShot)

case class AgentConstruct(externalBusy:Int,onCall:Int,wrapUp:Int)
case class DataShow()

case class HandleAgentEvent(extension:String,ready:Int,externalBusy:Int,onCall:Int,wrapUp:Int)


class Graph(ref:ActorRef) extends Actor with ActorLogging with FSM[AgentState,DataShow]{
  var extension:String=null
  var consumerSnapShot:CFISnapShot=null

  startWith(NotReady,DataShow())

  override def  preStart()={
      context.system.log.info(s"INITIALIZED FSM ${ref}")
  }
  when(NotReady){
    case Event(MoveNotReady(ext,consumer),_)=>
      extension=ext
      consumerSnapShot=consumer
      goto(NotReady)
    case Event(MoveReady(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Ready)
    case Event(MoveWrapUp(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(WrapUp)
    case Event(MoveBusy(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Busy)
  }
  when(Busy){
    case Event(MoveNotReady(ext,consumer),_)=>extension=ext
      consumerSnapShot=consumer
      goto(NotReady)
    case Event(MoveReady(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Ready)
    case Event(MoveWrapUp(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(WrapUp)
    case Event(MoveBusy(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Busy)
  }
  when(Ready){
    case Event(MoveNotReady(ext,consumer),_)=>extension=ext
      consumerSnapShot=consumer
      goto(NotReady)
    case Event(MoveReady(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Ready)
    case Event(MoveWrapUp(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(WrapUp)
    case Event(MoveBusy(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Busy)
  }
  when(WrapUp){
    case Event(MoveNotReady(ext,consumer),_)=>extension=ext
      consumerSnapShot=consumer
      goto(NotReady)
    case Event(MoveReady(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Ready)
    case Event(MoveWrapUp(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(WrapUp)
    case Event(MoveBusy(ext,consumer),_)=> extension=ext
      consumerSnapShot=consumer
      goto(Busy)
  }

  onTransition {

    case NotReady -> Busy =>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
        case Some(agentGraph)=>agentGraph.setPrevState(NotReady)
          agentGraph.setCurrentState(Busy)
          agentGraph.getQueues.foreach { z =>
            consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
              case Some(qG) => qG.setNotReadyCount(neglectNeg(qG.getNotReadyCount))
                qG.setBusyCount(qG.getBusyCount + 1)
              case None =>
            }
          }
          ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
        case None=>
      }

    case NotReady -> Ready=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
        case Some(agentGraph)=>agentGraph.setPrevState(NotReady)
          agentGraph.setCurrentState(Ready)
          agentGraph.getQueues.foreach {z=>
            consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
              case Some(qG)=> qG.setNotReadyCount(neglectNeg(qG.getNotReadyCount))
                qG.setReadyCount(qG.getReadyCount+1)
              case None=>
            }
          }
          ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
        case None=>
      }
    case NotReady -> WrapUp=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
        case Some(agentGraph)=>agentGraph.setPrevState(NotReady)
          agentGraph.setCurrentState(WrapUp)
          agentGraph.getQueues.foreach {z=>
            consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
              case Some(qG)=>qG.setNotReadyCount(neglectNeg(qG.getNotReadyCount))
                qG.setWrapUpCount(qG.getWrapUpCount+1)
              case None=>
            }
          }
          ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
        case None=>
      }
    case Busy -> NotReady=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(Busy)
            agentGraph.setCurrentState(NotReady)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setBusyCount(neglectNeg(qG.getBusyCount))
                  qG.setNotReadyCount(qG.getNotReadyCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

    case Busy -> Ready=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(Busy)
            agentGraph.setCurrentState(Ready)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setBusyCount(neglectNeg(qG.getBusyCount))
                  qG.setReadyCount(qG.getReadyCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

    case Busy -> WrapUp=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(Busy)
            agentGraph.setCurrentState(WrapUp)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setBusyCount(neglectNeg(qG.getBusyCount))
                  qG.setWrapUpCount(qG.getWrapUpCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }


    case Ready -> Busy=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(Ready)
            agentGraph.setCurrentState(Busy)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setReadyCount(neglectNeg(qG.getReadyCount))
                  qG.setBusyCount(qG.getBusyCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

    case Ready -> NotReady=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(Ready)
            agentGraph.setCurrentState(NotReady)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setReadyCount(neglectNeg(qG.getReadyCount))
                  qG.setNotReadyCount(qG.getNotReadyCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

    case Ready -> WrapUp=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(Ready)
            agentGraph.setCurrentState(WrapUp)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setReadyCount(neglectNeg(qG.getReadyCount))
                  qG.setWrapUpCount(qG.getWrapUpCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

    case WrapUp -> Ready=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(WrapUp)
            agentGraph.setCurrentState(Ready)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setWrapUpCount(neglectNeg(qG.getWrapUpCount))
                  qG.setReadyCount(qG.getReadyCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

    case WrapUp -> NotReady=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(WrapUp)
            agentGraph.setCurrentState(NotReady)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setWrapUpCount(neglectNeg(qG.getWrapUpCount))
                  qG.setNotReadyCount(qG.getNotReadyCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

    case WrapUp -> Busy=>
      consumerSnapShot._agentGraph.find(_.getExtension.equals(extension)) match {
          case Some(agentGraph)=>agentGraph.setPrevState(WrapUp)
            agentGraph.setCurrentState(Busy)
            agentGraph.getQueues.foreach {z=>
              consumerSnapShot._queueGraph.find(_.getExtension.equals(z)) match {
                case Some(qG)=>qG.setWrapUpCount(neglectNeg(qG.getWrapUpCount))
                  qG.setBusyCount(qG.getBusyCount+1)
                case None=>
              }
            }
            ref ! QueueGraphSnapShot(consumerSnapShot._queueGraph)
          case None=>
        }

  }

  initialize()

  def neglectNeg(a:Int):Int= a-1 < 0 match {
    case true=>0
    case false=>a-1
  }
}