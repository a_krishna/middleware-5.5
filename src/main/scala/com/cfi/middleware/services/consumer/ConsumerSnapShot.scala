package com.cfi.middleware.services.consumer

import java.util

import akka.actor.{ActorRef, ActorSystem}
import com.cfi.middleware.config.akka.{AkkaModule, ConfigModule}
import com.cfi.middleware.model._

import com.google.inject.{Guice, Injector}
import org.slf4j.Logger

import scala.collection.mutable

/**
  * Created by AravindKrishna on 5/12/15.
  */
class CFISnapShot {

import scala.collection.JavaConversions._
  val agentEvents = new mutable.HashSet[AgentEvent]
  val queueEvents = new mutable.HashSet[QueueEvent]
  val _agentStats = new mutable.HashSet[AgentStats]
  val _queueStats = new mutable.HashSet[QueueStats]
  val _queueGraph=new mutable.HashSet[QueueGraph]
  val _agentGraph=new mutable.HashSet[AgentGraph]
  val _LOGGER: Logger = org.slf4j.LoggerFactory.getLogger(classOf[CFISnapShot])

  def updateQueueEvent(objectMap: util.Map[String, Object], routing_key: String,queueEvent: QueueEvent): Option[QueueEvent] = {
    val state = objectMap.get("state").asInstanceOf[util.HashMap[String, Object]].toMap[String, Object]
    val action: String = objectMap.get("action").toString
        try {
          action match {
            case ("update" | "agent_ready" | "agent_not_ready") =>
              //          if (action.equals("agent_ready")) queueEvent.setAgents(state("agents").toString)
              queueEvent.setAvailable_count(state("agents_available").toString.toInt)
              queueEvent.setMsgType(action)
              Some(queueEvent)
            case "membership_update" =>
              queueEvent.setAgents(state("agents").toString)
              queueEvent.setMsgType(action)
              Some(queueEvent)
            case ("join" | "leave") =>
              queueEvent.setSessions(state("sessions").toString.toInt)
              queueEvent.setMsgType(action)
              Some(queueEvent)
            case _ => _LOGGER.debug("[ACTION NOT MATCHED " + " Action: " + action + " STATE : " + state + " ROUTING KEY" + routing_key)
              None
          }
        } catch {
          case exe: Exception =>
            _LOGGER.info(exe.getStackTrace + "[EXECEPTION OCCURED MATCHED " + routing_key + " Action: " + action + " STATE : " + state)
            None
        }
  }

  def updateAgentEvent(objectMap: util.Map[String, Object], routing_key: String,agentEvent: AgentEvent): Option[AgentEvent] = {
    val state = objectMap.get("state").asInstanceOf[util.HashMap[String, Object]].toMap[String, Object]
    val action: String = objectMap.get("action").toString
        try {
          action match {
            case "agent_away" | "agent_ready" =>
              if (action.equals("agent_away")) agentEvent.setReason(state("reason").toString)
              agentEvent.setReady(state("ready").toString.toInt)
              agentEvent.setOn_a_call(state("on_a_call").toString.toInt)
              agentEvent.setBegin_int(state("begin_int").toString.toInt)
              agentEvent.setMsgType(action)
              Some(agentEvent)
            case "offer_call" | "accept_call"  =>
              if (action.equals("accept_call") ) {
                agentEvent.setOn_a_call(state("on_a_call").toString.toInt)
                agentEvent.setBegin_int(state("begin_int").toString.toInt)
                agentEvent.setSession_count(state("session_count").toString.toInt)
              }
              agentEvent.setCall_callerid(state("call_callerid").toString.toInt)
              agentEvent.setCall_source(state("call_source").toString)
              agentEvent.setQueue_ext(state("queue_ext").toString.toInt)
              agentEvent.setQueue_name(state("queue_name").toString)
              agentEvent.setCurrent_uniqueid(state("current_uniqueid").toString)
              agentEvent.setMsgType(action)
              Some(agentEvent)
            case "hangup"=>
              agentEvent.setOn_a_call(state("on_a_call").toString.toInt)
              agentEvent.setBegin_int(state("begin_int").toString.toInt)
              agentEvent.setSession_count(state("session_count").toString.toInt)
              agentEvent.setQueue_name(state("queue_name").toString)
              agentEvent.setCall_source(state("call_source").toString)
              agentEvent.setCurrent_uniqueid(state("current_uniqueid").toString)
              agentEvent.getQueue_name match {
                case ""=>agentEvent.setQueue_ext(0)
                case _=>agentEvent.setQueue_ext(state("queue_ext").toString.toInt)
              }
              agentEvent.getCall_source match {
                case ""=>agentEvent.setCall_callerid(0)
                case _=> agentEvent.setCall_callerid(state("call_callerid").toString.toInt)
              }
              agentEvent.setOn_wrap_up(state("on_wrap_up").toString.toInt)
              agentEvent.setMsgType(action)
              Some(agentEvent)
            case "wrapup_complete"=>
              agentEvent.setOn_wrap_up(state("on_wrap_up").toString.toInt)
              agentEvent.setMsgType(action)
              Some(agentEvent)
            case "external_busy_update" =>
              agentEvent.setBegin_int(state("begin_int").toString.toInt)
              agentEvent.setExternal_busy(state("external_busy").toString.toInt)
              agentEvent.setMsgType(action)
              Some(agentEvent)
            case "logout" =>
              agentEvent.setReady(state("ready").toString.toInt)
              agentEvent.setReason(state("reason").toString)
              agentEvent.setMsgType(action)
              Some(agentEvent)
            case _ => _LOGGER.debug("[ACTION NOT MATCHED " + " Action: " + action + " STATE : " + state + " ROUTING KEY" + routing_key)
              None
          }
        } catch {
          case exe: Exception =>
            _LOGGER.info(exe.getStackTrace + "[EXECEPTION OCCURED MATCHED " + routing_key + " Action: " + action + " STATE : " + state)
            None
        }
  }

  def updateAgentStats(objectMap: util.Map[String, Object], routing_key: String,agentStats: AgentStats): Option[AgentStats] = {
    val state = objectMap.get("state").asInstanceOf[util.HashMap[String, Object]].toMap[String, Object]
    val action: String = objectMap.get("action").toString
        try {
          action match {
            case "update" =>
              agentStats.setHandled_in_last_day(state("handled_in_last_day").toString.toInt)
              agentStats.setMsgType(action)
              Some(agentStats)
            case _ => _LOGGER.debug("[ACTION NOT MATCHED AGENT STATS " + " Action: " + action + " STATE : " + state + " ROUTING KEY" + routing_key)
              None
          }
        } catch {
          case exe: Exception =>
            _LOGGER.info(exe.getStackTrace + "[EXECEPTION OCCURED MATCHED AGENT STATS" + routing_key + " Action: " + action + " STATE : " + state)
            agentStats.setAvailable_count(state("available_count").toString.toInt)
            agentStats.setMsgType(action)
            Some(agentStats)
        }
  }

  def updateQueueStats(objectMap: util.Map[String, Object], routing_key: String,queueStats: QueueStats): Option[QueueStats] = {
    val state = objectMap.get("state").asInstanceOf[util.HashMap[String, Object]].toMap[String, Object]
    val action: String = objectMap.get("action").toString
        try {
          action match {
            case "update" =>
              state.foreach(z => queueStats.checkAndAddField(z._1, z._2.toString))
              queueStats.setMsgType(action)
              Some(queueStats)
            case _ =>
              _LOGGER.info( "[QS ACTION NOT MATCHED " + " Action: " + action + " STATE : " + state + " ROUTING KEY" + routing_key)
              None
          }
        } catch {
          case exe: Exception =>
            _LOGGER.info( exe.getStackTrace + "[QS EXECEPTION OCCURED MATCHED " + routing_key + " Action: " + action + " STATE : " + state)
            None
        }
  }
}

object CFIConstant {
  val EXCHANGE_NAME_QUEUE_STATS: String = "cfi.queue.stats"
  val EXCHANGE_NAME_QUEUE_EVENT: String = "cfi.queue.event"
  val EXCHANGE_NAME_AGENT_EVENT: String = "cfi.agent.event"
  val EXCHANGE_NAME_AGENT_STATS: String = "cfi.agent.stats"
}

object CFI{
  val injector:Injector = Guice.createInjector(new ConfigModule, new AkkaModule)
  lazy val system:ActorSystem = injector.getInstance(classOf[ActorSystem])
  var managedActorRef:ActorRef=null
}


