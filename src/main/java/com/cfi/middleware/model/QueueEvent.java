package com.cfi.middleware.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by AravindKrishna on 2/12/15.
 * <p>
 * 1) extension (501)
 * 2) extname (501)
 * 3) sessions (0 or 1)
 * 4) external_busy (0 or 1)
 * 5) agents_available ( count of agents available to that queue)
 */
public class QueueEvent {
    String extension;
    String extname;
    Integer sessions;
    Integer agents_available;
    String agents;
    private String msgType;
    private Integer available_count;
    private Integer abandoned_in_last_day;
    private Integer agent_count;
    private Integer handled_in_last_day;
    private Integer offered_in_last_day;

    public QueueEvent() {

    }

    public QueueEvent(String extension, String extname, Integer sessions, Integer agents_available, String agents) {
        this.extension = extension;
        this.extname = extname;
        this.sessions = sessions;
        this.agents_available = agents_available;
        this.agents = agents;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public void setAvailable_count(Integer available_count) {
        this.available_count = available_count;
    }

    public void setExtname(String extname) {
        this.extname = extname;
    }

    public void setSessions(Integer sessions) {
        this.sessions = sessions;
    }

    public void setAgents_available(Integer agents_available) {
        this.agents_available = agents_available;
    }

    public void setAgents(String agents) {
        this.agents = agents;
    }

    public Map<String, Object> getSnapShotdata() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("extension", extension);
        data.put("extname", extname);
        data.put("sessions", sessions);
        data.put("agents_available", agents_available);
        data.put("available_count", available_count);
        data.put("action", msgType);
        data.put("agents",agents);
        data.put("abandoned_in_last_day",abandoned_in_last_day);
        data.put("agent_count",agent_count);
        data.put("handled_in_last_day",handled_in_last_day);
        data.put("offered_in_last_day",offered_in_last_day);
        return data;
    }

    public Integer getAbandoned_in_last_day() {
        return abandoned_in_last_day;
    }

    public void setAbandoned_in_last_day(Integer abandoned_in_last_day) {
        this.abandoned_in_last_day = abandoned_in_last_day;
    }

    public Integer getAgent_count() {
        return agent_count;
    }

    public void setAgent_count(Integer agent_count) {
        this.agent_count = agent_count;
    }

    public Integer getHandled_in_last_day() {
        return handled_in_last_day;
    }

    public void setHandled_in_last_day(Integer handled_in_last_day) {
        this.handled_in_last_day = handled_in_last_day;
    }

    public Integer getOffered_in_last_day() {
        return offered_in_last_day;
    }

    public void setOffered_in_last_day(Integer offered_in_last_day) {
        this.offered_in_last_day = offered_in_last_day;
    }

    @Override
    public String toString() {
        return "QueueEvent{" +
                "extension='" + extension + '\'' +
                ", extname='" + extname + '\'' +
                ", sessions=" + sessions +
                ", agents_available=" + agents_available +
                ", agents='" + agents + '\'' +
                ", msgType='" + msgType + '\'' +
                ", available_count=" + available_count +
                ", abandoned_in_last_day=" + abandoned_in_last_day +
                ", agent_count=" + agent_count +
                ", handled_in_last_day=" + handled_in_last_day +
                ", offered_in_last_day=" + offered_in_last_day +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueueEvent that = (QueueEvent) o;

        return extension.equals(that.extension);

    }

    @Override
    public int hashCode() {
        return extension.hashCode();
    }
}
