package com.cfi.middleware.model;

/**
 * Created by AravindKrishna on 5/12/15.
 */
public class Transaction {

    private String transactionId;
    private String process;
    private String command;
    private String commandType;
    private Object message;

    public Transaction() {

    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getCommandType() {
        return commandType;
    }

    public void setCommandType(String commandType) {
        this.commandType = commandType;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId='" + transactionId + '\'' +
                ", command='" + command + '\'' +
                ", commandType='" + commandType + '\'' +
                ", message=" + message +
                '}';
    }
}
