package com.cfi.middleware.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by AravindKrishna on 2/12/15.
 * 1) ready ( 0 or 1)
 * 2) reason (String) (New login, Break,Login, Logout)
 * 3) extension (101)
 * 4) agent_name (101)
 * 5) on_a_call (0 or 1)
 * 6) external_busy ( 0 or 1)
 * 7) queues String=> Seq[Int] (501,502,503,504)
 * 8) begin_int (Integer)
 * 9) held_call_id (give details of the call going)
 * {queue_name=Q503, call_callerid=102, queue_ext=503, current_uniqueid=CAS-7DJT9U6UG7LD, call_cnam=102 102, on_wrap_up=0, queue_session=QSE-7DJT9U73B5D7, session_id=CAS-7DJT9U6UG7LD, agent_session_id=AGS-7DJSWHHGV2SE, time=Fri Dec 04 15:07:28 IST 2015, call_source=102 102 - 102}
 * {on_a_call=1, queue_name=IBVQ, call_callerid=101, current_uniqueid=CAS-7DJQU9U2ZHSA, queue_ext=501, call_cnam=101 101, session_count=1, queue_session=QSE-7DJQU9UEQYH3, time=Fri Dec 04 14:04:13 IST 2015, begin_int=1449218053, agent_session_id=AGS-7DJPDEG760T0, call_source=101 101 – 101}
 * {queue_ext=, call_cnam=, begin_int=1449218207, on_a_call=0, queue_name=, call_callerid=, current_uniqueid=CAS-7DJQU9U2ZHSA, session_count=0, on_wrap_up=1, queue_session=QSE-7DJQU9UEQYH3, time=Fri Dec 04 14:06:47 IST 2015, agent_session_id=AGS-7DJPDEG760T0, call_source=}
 */
public class AgentEvent {
    Integer ready;
    String reason;
    String extension;
    String agent_name;
    Integer on_a_call;
    Integer external_busy;
    String queues;
    Integer begin_int;
    Integer session_count;
    String held_call_id;
    private String msgType;
    private String queue_name;
    private Integer call_callerid;
    private Integer queue_ext;
    private String call_source;
    private String fullname;
    private Integer handled_in_last_day;
    private Integer on_wrap_up;
    private String current_uniqueid;


    public AgentEvent() {
    }

    public AgentEvent(Integer ready, String reason, String extension, String agent_name, Integer on_a_call, Integer external_busy, String queues, Integer begin_int, Integer session_count, String held_call_id) {
        this.ready = ready;
        this.reason = reason;
        this.extension = extension;
        this.agent_name = agent_name;
        this.on_a_call = on_a_call;
        this.external_busy = external_busy;
        this.session_count = session_count;
        this.queues = queues;
        this.begin_int = begin_int;
        this.held_call_id = held_call_id;
    }

    public void setReady(Integer ready) {
        this.ready = ready;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public void setOn_a_call(Integer on_a_call) {
        this.on_a_call = on_a_call;
    }

    public void setExternal_busy(Integer external_busy) {
        this.external_busy = external_busy;
    }

    public void setQueues(String queues) {
        this.queues = queues;
    }

    public void setBegin_int(Integer begin_int) {
        this.begin_int = begin_int;
    }

    public void setSession_count(Integer session_count) {
        this.session_count = session_count;
    }

    public void setHeld_call_id(String held_call_id) {
        this.held_call_id = held_call_id;
    }

    public String getExtension() {
        return extension;
    }

    public String getQueues() {
        return queues;
    }

    public String getCurrent_uniqueid() {
        return current_uniqueid;
    }

    public void setCurrent_uniqueid(String current_uniqueid) {
        this.current_uniqueid = current_uniqueid;
    }

    public Integer getReady() {
        if(ready==null){
            return  0;
        }else {
            return ready;
        }
    }

    public Integer getOn_a_call() {
        if(on_a_call==null){
            return  0;
        }else {
            return on_a_call;
        }
    }

    public Integer getExternal_busy() {
        if(external_busy==null){
            return  0;
        }else {
            return external_busy;
        }
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public void setQueue_name(String queue_name) {
        this.queue_name = queue_name;
    }

    public void setCall_callerid(Integer call_callerid) {
        this.call_callerid = call_callerid;
    }

    public void setQueue_ext(Integer queue_ext) {
        this.queue_ext = queue_ext;
    }

    public void setCall_source(String call_source) {
        this.call_source = call_source;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getHandled_in_last_day() {
        return handled_in_last_day;
    }

    public void setHandled_in_last_day(Integer handled_in_last_day) {
        this.handled_in_last_day = handled_in_last_day;
    }

    public Integer getOn_wrap_up() {
        if(on_wrap_up==null){
            return  0;
        }else {
            return on_wrap_up;
        }
    }

    public void setOn_wrap_up(Integer on_wrap_up) {
        this.on_wrap_up = on_wrap_up;
    }

    public String getCall_source() {
        return call_source;
    }

    public String getQueue_name() {
        return queue_name;
    }

    public Map<String, Object> getSnapsotData() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("ready", ready);
        data.put("reason", reason);
        data.put("extension", extension);
        data.put("agent_name", agent_name);
        data.put("on_a_call", on_a_call);
        data.put("external_busy", external_busy);
        data.put("session_count", session_count);
        data.put("queues", queues);
        data.put("begin_int", begin_int);
        data.put("held_call_id", held_call_id);
        data.put("fullname", fullname);
        data.put("action", msgType);
        data.put("handled_in_last_day",handled_in_last_day);
        data.put("current_uniqueid",current_uniqueid);
        return data;
    }

    public Map<String, Object> getCallDetails() {
        Map<String, Object> data = getSnapsotData();
        data.put("call_callerid", call_callerid);
        data.put("call_source", call_source);
        data.put("queue_ext", queue_ext);
        data.put("queue_name", queue_name);
        data.put("on_wrap_up",on_wrap_up);
        return data;
    }

    @Override
    public String toString() {
        return "AgentEvent{" +
                "ready=" + ready +
                ", reason='" + reason + '\'' +
                ", extension='" + extension + '\'' +
                ", agent_name='" + agent_name + '\'' +
                ", on_a_call=" + on_a_call +
                ", external_busy=" + external_busy +
                ", queues='" + queues + '\'' +
                ", begin_int=" + begin_int +
                ", session_count=" + session_count +
                ", held_call_id='" + held_call_id + '\'' +
                ", msgType='" + msgType + '\'' +
                ", queue_name='" + queue_name + '\'' +
                ", call_callerid=" + call_callerid +
                ", queue_ext=" + queue_ext +
                ", call_source='" + call_source + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgentEvent that = (AgentEvent) o;

        return extension.equals(that.extension);

    }

    @Override
    public int hashCode() {
        return extension.hashCode();
    }
}
