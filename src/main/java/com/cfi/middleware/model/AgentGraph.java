package com.cfi.middleware.model;

import com.cfi.middleware.services.consumer.graph.AgentState;

import java.util.Arrays;

/**
 * Created by AravindKrishna on 10/12/15.
 */
public class AgentGraph {
    String extension;
    AgentState prevState;
    AgentState currentState;
    String[] queues;

    public AgentGraph(){

    }

    public AgentGraph(String extension,AgentState state){
        this.extension=extension;
        this.currentState= this.prevState=state;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public AgentState getPrevState() {
        return prevState;
    }

    public void setPrevState(AgentState prevState) {
        this.prevState = prevState;
    }

    public AgentState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(AgentState currentState) {
        this.currentState = currentState;
    }

    public String[] getQueues() {
        return queues;
    }

    public void setQueues(String[] queues) {
        this.queues = queues;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgentGraph that = (AgentGraph) o;

        return extension.equals(that.extension);

    }

    @Override
    public int hashCode() {
        return extension.hashCode();
    }

    @Override
    public String toString() {
        return "AgentGraph{" +
                "extension='" + extension + '\'' +
                ", prevState=" + prevState +
                ", currentState=" + currentState +
                ", queues=" + Arrays.toString(queues) +
                '}';
    }
}
