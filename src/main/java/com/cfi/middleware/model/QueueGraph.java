package com.cfi.middleware.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by AravindKrishna on 10/12/15.
 */
public class QueueGraph {
    String extension;
    Integer wrapUpCount;
    Integer busyCount;
    Integer readyCount;
    Integer notReadyCount;
    String action;

    public QueueGraph(){

    }
    public QueueGraph(String extension,Integer _default){
        this.extension=extension;
        this.busyCount=this.wrapUpCount=this.notReadyCount=this.readyCount=_default;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getWrapUpCount() {
        return wrapUpCount;
    }

    public void setWrapUpCount(Integer wrapUpCount) {
        this.wrapUpCount = wrapUpCount;
    }

    public Integer getBusyCount() {
        return busyCount;
    }

    public void setBusyCount(Integer busyCount) {
        this.busyCount = busyCount;
    }

    public Integer getReadyCount() {
        return readyCount;
    }

    public void setReadyCount(Integer readyCount) {
        this.readyCount = readyCount;
    }

    public Integer getNotReadyCount() {
        return notReadyCount;
    }

    public void setNotReadyCount(Integer notReadyCount) {
        this.notReadyCount = notReadyCount;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Map<String,Object> getSnapShot(){
      Map<String,Object>  objectMap=new HashMap<String,Object>();
      objectMap.put("EXT",extension+"");
      objectMap.put("AGENT BUSY",busyCount+"");
      objectMap.put("AGENT READY",readyCount+"");
      objectMap.put("AGENT AWAY",notReadyCount+"");
      objectMap.put("AGENT IN WRAPUP",wrapUpCount+"");
      return objectMap;
  }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueueGraph that = (QueueGraph) o;

        return extension.equals(that.extension);

    }

    @Override
    public int hashCode() {
        return extension.hashCode();
    }
}
