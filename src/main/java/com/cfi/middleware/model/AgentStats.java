package com.cfi.middleware.model;

/**
 * Created by AravindKrishna on 3/12/15.
 * {"available_count":[0-9],"handled_in_last_day":[0-9],"queue_count":[0-9]}
 */
public class AgentStats {
    Integer available_count;
    Integer handled_in_last_day;
    Integer queue_count;
    private String msgType;
    private String extension;

    public AgentStats() {

    }

    public AgentStats(Integer available_count, Integer handled_in_last_day, Integer queue_count) {
        this.available_count = available_count;
        this.handled_in_last_day = handled_in_last_day;
        this.queue_count = queue_count;
    }

    public void setAvailable_count(Integer available_count) {
        this.available_count = available_count;
    }

    public void setHandled_in_last_day(Integer handled_in_last_day) {
        this.handled_in_last_day = handled_in_last_day;
    }

    public void setQueue_count(Integer queue_count) {
        this.queue_count = queue_count;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getAvailable_count() {
        return available_count;
    }

    public Integer getHandled_in_last_day() {
        return handled_in_last_day;
    }

    public Integer getQueue_count() {
        return queue_count;
    }

    @Override
    public String toString() {
        return "AgentStats{" +
                "available_count=" + available_count +
                ", handled_in_last_day=" + handled_in_last_day +
                ", queue_count=" + queue_count +
                ", msgType='" + msgType + '\'' +
                ", extension=" + extension +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgentStats that = (AgentStats) o;

        return extension.equals(that.extension);

    }

    @Override
    public int hashCode() {
        return extension.hashCode();
    }
}
