package com.cfi.middleware.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by AravindKrishna on 3/12/15.
 * "abandoned_in_last_day":0,"agent_count":1,"available_count":0,"handled_in_last_day":0,"max_wait":0,"offered_in_last_day":0
 */
public class QueueStats {
    Integer abandoned_in_last_day;
    Integer agent_count;
    Integer available_count;
    Integer handled_in_last_day;
    Integer max_wait;
    Integer offered_in_last_day;
    private String msgType;
    private String extension;

    public QueueStats() {

    }

    public QueueStats(Integer abandoned_in_last_day, Integer agent_count, Integer available_count, Integer handled_in_last_day, Integer max_wait, Integer offered_in_last_day) {
        this.abandoned_in_last_day = abandoned_in_last_day;
        this.agent_count = agent_count;
        this.available_count = available_count;
        this.handled_in_last_day = handled_in_last_day;
        this.max_wait = max_wait;
        this.offered_in_last_day = offered_in_last_day;
    }

    public Integer getAbandoned_in_last_day() {
        return abandoned_in_last_day;
    }

    public Integer getAgent_count() {
        return agent_count;
    }

    public Integer getAvailable_count() {
        return available_count;
    }

    public Integer getHandled_in_last_day() {
        return handled_in_last_day;
    }

    public Integer getMax_wait() {
        return max_wait;
    }

    public Integer getOffered_in_last_day() {
        return offered_in_last_day;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public void setAbandoned_in_last_day(Integer abandoned_in_last_day) {
        this.abandoned_in_last_day = abandoned_in_last_day;
    }

    public void setAgent_count(Integer agent_count) {
        this.agent_count = agent_count;
    }

    public void setAvailable_count(Integer available_count) {
        this.available_count = available_count;
    }

    public void setHandled_in_last_day(Integer handled_in_last_day) {
        this.handled_in_last_day = handled_in_last_day;
    }

    public void setMax_wait(Integer max_wait) {
        this.max_wait = max_wait;
    }

    public void setOffered_in_last_day(Integer offered_in_last_day) {
        this.offered_in_last_day = offered_in_last_day;
    }

    public void checkAndAddField(String key, String keyValue) {
        if (key.equals("abandoned_in_last_day")) {
            setAbandoned_in_last_day(Integer.valueOf(keyValue));
        } else if (key.equals("agent_count")) {
            setAgent_count( Integer.valueOf(keyValue));
        } else if (key.equals("available_count")) {
            setAvailable_count(Integer.valueOf(keyValue));
        } else if (key.equals("handled_in_last_day")) {
            setHandled_in_last_day(Integer.valueOf(keyValue));
        } else if (key.equals("max_wait")) {
           setMax_wait( Integer.valueOf(keyValue));
        } else if (key.equals("offered_in_last_day")) {
           setOffered_in_last_day( Integer.valueOf(keyValue));
        }else{
            System.out.println(key+" "+keyValue);
        }
    }

    public Map<String,Object> getMapData(){
        Map<String,Object> objectMap=new HashMap<String, Object>();
        objectMap.put("extension",extension);
        objectMap.put("max_wait",max_wait);
        objectMap.put("agent_count",agent_count);
        objectMap.put("handled_in_last_day",handled_in_last_day);
        objectMap.put("offered_in_last_day",offered_in_last_day);
        objectMap.put("abandoned_in_last_day",abandoned_in_last_day);
        objectMap.put("available_count",available_count);
        objectMap.put("msgType",msgType);
        return objectMap;
    }

    @Override
    public String toString() {
        return "QueueStats{" +
                "abandoned_in_last_day=" + abandoned_in_last_day +
                ", agent_count=" + agent_count +
                ", available_count=" + available_count +
                ", handled_in_last_day=" + handled_in_last_day +
                ", max_wait=" + max_wait +
                ", offered_in_last_day=" + offered_in_last_day +
                ", msgType='" + msgType + '\'' +
                ", extension=" + extension +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueueStats that = (QueueStats) o;

        return !(extension != null ? !extension.equals(that.extension) : that.extension != null);

    }

    @Override
    public int hashCode() {
        return extension != null ? extension.hashCode() : 0;
    }
}
