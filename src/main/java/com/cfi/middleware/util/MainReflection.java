package com.cfi.middleware.util;


import com.cfi.middleware.model.AgentEvent;
import com.cfi.middleware.model.QueueEvent;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by AravindKrishna on 2/12/15.
 */
public class MainReflection {


    public MainReflection() {
    }

    public static Object classMapper(String className, Map<String, Object> headers) {
        Object _instance = null;
        try {
            Class cls = Class.forName(className);
            int size = 0;
            for (Constructor constructor : cls.getConstructors()) {
                if (constructor.getParameterCount() > 1) {
                    size = constructor.getParameterCount();
                }
            }
            Class paramType[] = new Class[size];
            Object paramValue[] = new Object[size];
            List<Field> fullAccessFields = new ArrayList<Field>(size);
            for (Field field : cls.getDeclaredFields()) {
                if (!Modifier.toString(field.getModifiers()).contains("private")) {
                    fullAccessFields.add(field);
                }
            }
            for (int i = 0; i < size; i++) {
                Field field = fullAccessFields.get(i);
                if (field.getName().equals("extension")) {
                    paramType[i] = String.class;
                } else {
                    paramType[i] = field.getType();
                }

                try {
                    if (field.getName().equals("extension")) {
                        paramValue[i] = headers.get(field.getName()).toString();
                    } else {
                        paramValue[i] = field.getType().cast(headers.get(field.getName()));
                    }
                } catch (ClassCastException e) {
                    if (e.getMessage().contains("java.lang.String")) {
                        paramValue[i] = new String(headers.get(field.getName()).toString());
                    } else if (e.getMessage().contains("java.lang.Integer")) {
                        paramValue[i] = Integer.valueOf(headers.get(field.getName()).toString());
                    }
                }
            }
            Constructor ct = cls.getConstructor(paramType);
            _instance = ct.newInstance(paramValue);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return _instance;
    }

    public static void main(String args[]) {
        //{extension=505, sessions=0, extname=Q505, agents=[100, 101, 102, 103], agents_available=0}
        Map<String, Object> mapper = new HashMap<String, Object>();
        Map<String, Object> qE = new HashMap<String, Object>();
        qE.put("extension", 505);
        qE.put("sessions", 0);
        qE.put("extname", "Q505");
        qE.put("agents", "[100,101,102,103,104]");
        qE.put("agents_available", 0);
        mapper.put("ready", 0);
        mapper.put("reason", "Unavailable");
        mapper.put("extension", 103);
        mapper.put("agent_name", "Sanjay Patel");
        mapper.put("on_a_call", 0);
        mapper.put("external_busy", 0);
        mapper.put("queues", "501,502,503,505");
        mapper.put("begin_int", 1448617657);
        mapper.put("held_call_id", "");
        int sap = 103;
        MainReflection mainReflection = new MainReflection();
        AgentEvent agentEvent = (AgentEvent) mainReflection.classMapper("com.cfi.middleware.model.AgentEvent", mapper);
        QueueEvent queueEvent = (QueueEvent) mainReflection.classMapper("com.cfi.middleware.model.QueueEvent", qE);
        System.out.println(agentEvent.toString());
        System.out.println(queueEvent.toString());
        System.out.println(args[0].matches("[\\d].*"));
        System.out.println(agentEvent.getExtension().equals(sap + ""));
        System.out.println(agentEvent.getExtension());
    }
}
