package com.cfi.middleware.config.atmosphere;

import java.lang.annotation.*;

/**
 * Created by AravindKrishna on 1/12/15.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomConfig {
}
