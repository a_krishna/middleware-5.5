package com.cfi.middleware.config.atmosphere;

import org.atmosphere.annotation.Processor;
import org.atmosphere.config.AtmosphereAnnotation;
import org.atmosphere.cpr.AtmosphereFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by AravindKrishna on 1/12/15.
 */
@AtmosphereAnnotation(CustomConfig.class)
public class SampleConfig implements Processor<Object> {

    private Logger logger = LoggerFactory.getLogger(SampleConfig.class);

    @Override
    public void handle(AtmosphereFramework atmosphereFramework, Class<Object> aClass) {
        logger.info("Custom annotation {} discovered. Starting the Chat Sample", aClass.getAnnotation(CustomConfig.class));
    }
}
