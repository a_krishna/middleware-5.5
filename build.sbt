name := "middleware-5.0"

version := "1.5"

scalaVersion := "2.11.6"

lazy val rest = (project in file(".")).enablePlugins(TomcatPlugin)

libraryDependencies ++= Seq("org.atmosphere" % "atmosphere-runtime" % "2.4.1",
  "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided",
  "org.atmosphere" % "atmosphere-annotations" % "2.4.1",
  "org.mortbay.jetty" % "jetty-runner" % "7.0.0.v20091005" intransitive(),
  "org.slf4j" % "slf4j-simple" % "1.7.5",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.1",
  "com.typesafe.akka" %% "akka-slf4j" % "2.3.9",
  "com.rabbitmq" % "amqp-client" % "3.5.6",
  "com.google.inject" % "guice" % "4.0-beta5",
  "net.codingwell" %% "scala-guice" % "4.0.0-beta5",
  "com.typesafe.slick" %% "slick-extensions" % "3.0.0",
  "mysql" % "mysql-connector-java" % "5.1.36",
  "joda-time" % "joda-time" % "2.8.1",
    "com.typesafe.akka" %% "akka-actor" % "2.3.9")

containerMain := "org.mortbay.jetty.runner.Runner"

classDirectory in Compile := file("src/main/webapp/WEB-INF/classes")

resourceDirectory in Compile := file("src/main/resources")

resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/maven-releases/"


